// Sriramajayam

// Run as:
// ./testP2P1 -ksp_type fgmres -pc_type fieldsplit -pc_fieldsplit_type schur -pc_fieldsplit_schur_fact_type lower -fieldsplit_1_pc_type none

#include <stk_StokesProblem.h>
#include <geom_LevelSetManifold.h>
#include <msh_StdTriMesh.h>
#include <msh_TriangleSubdivision.h>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <map>

// Create a mesh of equilateral triangles
void GetEquilateralMesh(const int nDiv,
			std::vector<double>& coordinates,
			std::vector<int>& connectivity);

// Level set function for a circle
void CircleFunc(const double* X, double& F,
		double* dF, double* d2F, void* params);

// Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// exact solution
void GetExactSolution(const double* X, double* UP);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a mesh of equilateral triangles
  std::vector<double> coordinates;
  std::vector<int> connectivity;
  GetEquilateralMesh(5, coordinates, connectivity);
  msh::SeqCoordinates Coord(2,coordinates);
  msh::StdTriConnectivity Conn(connectivity);
  msh::StdTriMesh BG(Coord, Conn);
  msh::PlotTecStdTriMesh("BG.tec", BG);
  
  // Create geometry of the circle
  geom::NLSolverParams nlparams({.ftol=1.e-6, .ttol=1.e-6, .max_iter=25});
  geom::LevelSetFunction LSFunc = CircleFunc;
  geom::LevelSetManifold<2> Geom(nlparams, LSFunc);

  // Meshing options
  stk::Meshing_Options mesh_options({.nMaxThreads=1,
	.MaxVertValency=6, .MaxElmValency=6, .nSteps=5, .nIters=8, .nBdSamples=0});

  // Viscosity
  const double mu = 1.;
  
  // Stokes problem
  stk::StokesProblem SP(mu, BG, Geom, mesh_options);

  // Iterate
  for(int iter=0; iter<1; ++iter)
    {
      std::cout<<"\nIteration "<<iter<<std::flush;

      // Setup the iteration
      SP.SetupIteration();
      const auto& MD = SP.GetConformingMesh();
      PlotTecCoordConn((char*)"MD.tec", MD);
      const auto& L2GMap = SP.GetLocalToGlobalMap();
      
      // boundary conditions
      stk::BCParams bcs;
      GetDirichletBCs(MD, L2GMap, bcs.dirichlet_dofs, bcs.dirichlet_values);

      // Solve
      SP.Compute(bcs);

      // Plot the state
      std::string filename = "sol-"+std::to_string(iter)+".tec";
      SP.PlotState(filename);

      // Compute net forces and moments
      double Frc[2], Moment[1];
      double origin[] = {0.,0.};
      SP.ComputeForcesAndMoments(Frc, origin, Moment);
      std::cout<<"\nForce = "<<Frc[0]<<", "<<Frc[1]
	       <<"\nMoment = "<<Moment[0]<<std::flush;

      // Finalize this iteration
      SP.FinalizeIteration();
    }
  
  // Finalize PETSc
  PetscFinalize();
}  


// Create a mesh of equilateral triangles
void GetEquilateralMesh(const int nDiv,
			std::vector<double>& coordinates,
			std::vector<int>& connectivity)
{
  connectivity = std::vector<int>({0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1});
  coordinates = std::vector<double>({0.,0.});
  const double PI = 4.*std::atan(1.);
  for(int i=0; i<6; ++i)
    { double angle = 2.*PI*static_cast<double>(i)/6.;
      coordinates.push_back( std::cos(angle) );
      coordinates.push_back( std::sin(angle) ); }
  for(int i=0; i<nDiv; ++i)
    msh::SubdivideTriangles(2, connectivity, coordinates);
  return;
}

// Level set function for a circle
void CircleFunc(const double* X, double& F,
		double* dF, double* d2F, void* params)
{
  const double rad2 = 0.4*0.4;
  double r2 = X[0]*X[0]+X[1]*X[1];
  F = r2-rad2;
  if(dF!=nullptr)
    { dF[0] = 2.*X[0]; dF[1] = 2.*X[1]; }
  if(d2F!=nullptr)
    { d2F[0] = 2.; d2F[1] = 0.;
      d2F[2] = 0.; d2F[2] = 2.; }
  return;
}



// Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  std::map<int, double> bdofs2bvalues({});
  std::vector<std::vector<int>> ElmNbs;
  GetCoordConnFaceNeighborList(MD, ElmNbs);
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(ElmNbs[e][2*f]<0)
	{
	  const int* conn = &MD.connectivity[3*e];
	  int n0 = conn[f]-1;
	  int n1 = conn[(f+1)%3]-1;
	  const double* X0 = &MD.coordinates[2*n0];
	  const double* X1 = &MD.coordinates[2*n1];
	  const double X2[] = {0.5*(X0[0]+X1[0]), 0.5*(X0[1]+X1[1])};

	  int locnodes[] = {f, (f+1)%3, f+3};
	  double UP[9];
	  GetExactSolution(X0, UP);
	  GetExactSolution(X1, UP+3);
	  GetExactSolution(X2, UP+6);

	  for(int i=0; i<3; ++i)
	    for(int field=0; field<2; ++field)
	    bdofs2bvalues[L2GMap.Map(field,locnodes[i],e)] = UP[3*i+field];
	}

  // Fix the pressure at one point
  { int e = MD.elements/2;
    int a = 0;
    const int n = MD.connectivity[3*e+a]-1;
    const double* X = &MD.coordinates[2*n];
    double UP[3];
    GetExactSolution(X, UP);
    bdofs2bvalues[L2GMap.Map(2,a,e)] = UP[2]; }

  boundary.clear();
  bvalues.clear();
  for(auto& it:bdofs2bvalues)
    { boundary.push_back(it.first);
      bvalues.push_back(it.second); }

  return;
}


// exact solution
void GetExactSolution(const double* X, double* UP)
{
  const double& x = X[0];
  const double& y = X[1];

  //velocity
  UP[0] = std::exp(x)*((x-1.)*std::cos(y) + y*std::sin(y))/2.;
  UP[1] = std::exp(x)*(y*std::cos(y) - (x+1.)*std::sin(y))/2.;

  //pressure
  UP[2] = 2.*std::exp(x)*std::cos(y);

  return;
}

