// Sriramajayam

#include <stk_StabilizedStokesProblem.h>
#include <geom_LevelSetManifold.h>
#include <msh_StdTriMesh.h>
#include <msh_TriangleSubdivision.h>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <map>

// Create a mesh of equilateral triangles
double GetEquilateralMesh(const int nDiv,
			  std::vector<double>& coordinates,
			  std::vector<int>& connectivity);

// Level set function for a circle
void CircleFunc(const double* X, double& F,
		double* dF, double* d2F, void* params);

// Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// exact solution
void GetExactSolution(const double* X, double* UP);


int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a mesh of equilateral triangles
  std::vector<double> coordinates;
  std::vector<int> connectivity;
  const double meshsize = GetEquilateralMesh(6, coordinates, connectivity);
  msh::SeqCoordinates Coord(2,coordinates);
  msh::StdTriConnectivity Conn(connectivity);
  msh::StdTriMesh BG(Coord, Conn);
  msh::PlotTecStdTriMesh("BG.tec", BG);
  
  // Create geometry of the circle
  geom::NLSolverParams nlparams({.ftol=1.e-6, .ttol=1.e-6, .max_iter=25});
  geom::LevelSetFunction LSFunc = CircleFunc;
  geom::LevelSetManifold<2> Geom(nlparams, LSFunc);

  // Meshing options
  stk::Meshing_Options mesh_options({.nMaxThreads=1,
	.MaxVertValency=6, .MaxElmValency=6, .nSteps=5, .nIters=8, .nBdSamples=0});

  // Viscosity
  const double mu = 1.;
  
  // Stabilized stokes problem
  stk::StabStokesProblem SP(mu, meshsize, BG, Geom, mesh_options);

  // Iterate
  for(int iter=0; iter<1; ++iter)
    {
      std::cout<<"\nIteration "<<iter<<std::flush;

      // Setup the iteration
      SP.SetupIteration();
      const auto& MD = SP.GetConformingMesh();
      PlotTecCoordConn((char*)"MD.tec", MD);
      const auto& L2GMap = SP.GetLocalToGlobalMap();
      
      // boundary conditions
      stk::BCParams bcs;
      GetDirichletBCs(MD, L2GMap, bcs.dirichlet_dofs, bcs.dirichlet_values);

      // Solve
      SP.Compute(bcs);

      // Plot the state
      std::string filename = "sol-"+std::to_string(iter)+".tec";
      SP.PlotState(filename);

      // Compute the net force and moment
      double Frc[2], Moment[1];
      double origin[] = {0.,0.};
      SP.ComputeNetForceAndMoment(Frc, origin, Moment);
      std::cout<<"\nForce = "<<Frc[0]<<", "<<Frc[1]
	       <<"\nMoment = "<<Moment[0]<<std::flush;
      
      // Finalize this iteration
      SP.FinalizeIteration();
    }
  
   // Finalize PETSc
  PetscFinalize();
} 
  

// Create a mesh of equilateral triangles
double GetEquilateralMesh(const int nDiv,
			std::vector<double>& coordinates,
			std::vector<int>& connectivity)
{
  double h = 1.;
  connectivity = std::vector<int>({0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1});
  coordinates = std::vector<double>({0.,0.});
  const double PI = 4.*std::atan(1.);
  for(int i=0; i<6; ++i)
    { double angle = 2.*PI*static_cast<double>(i)/6.;
      coordinates.push_back( std::cos(angle) );
      coordinates.push_back( std::sin(angle) ); }
  for(int i=0; i<nDiv; ++i)
    { msh::SubdivideTriangles(2, connectivity, coordinates);
      h /= 2.; }
  return h;
}

// Level set function for a circle
void CircleFunc(const double* X, double& F,
		double* dF, double* d2F, void* params)
{
  const double rad2 = 0.4*0.4;
  double r2 = X[0]*X[0]+X[1]*X[1];
  F = r2-rad2;
  if(dF!=nullptr)
    { dF[0] = 2.*X[0]; dF[1] = 2.*X[1]; }
  if(d2F!=nullptr)
    { d2F[0] = 2.; d2F[1] = 0.;
      d2F[2] = 0.; d2F[2] = 2.; }
  return;
}


// exact solution
void GetExactSolution(const double* X, double* UP)
{
  const double& x = X[0];
  const double& y = X[1];

  //velocity
  UP[0] = std::exp(x)*((x-1.)*std::cos(y) + y*std::sin(y))/2.;
  UP[1] = std::exp(x)*(y*std::cos(y) - (x+1.)*std::sin(y))/2.;

  //pressure
  UP[2] = 2.*std::exp(x)*std::cos(y);

  return;
}

// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  std::set<int> bdofs({});
  std::vector<std::vector<int>> ElmNbs;
  GetCoordConnFaceNeighborList(MD, ElmNbs);
  double UP[3];
  
  std::map<int, double> bdofs2bvalues({});
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(ElmNbs[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  {
	    const int a = (f+i)%3;
	    const int n = MD.connectivity[3*e+a]-1;
	    const double* X = &MD.coordinates[2*n];
	    GetExactSolution(X, UP);
	    for(int field=0; field<2; ++field)
	      bdofs2bvalues[L2GMap.Map(field, a, e)] = UP[field];
	  }

  // Fix pressure at one point
  { int e = MD.elements/2;
    int a = 0;
    const int n = MD.connectivity[3*e+a]-1;
    const double* X = &MD.coordinates[2*n];
    GetExactSolution(X, UP);
    bdofs2bvalues[L2GMap.Map(2,a,e)] = UP[2]; }
  
  boundary.clear();
  bvalues.clear();
  for(auto& it:bdofs2bvalues)
    { boundary.push_back( it.first );
      bvalues.push_back( it.second ); }

  return;
}

