// Sriramajayam

#include <stk_StokesOperation.h>
#include <stk_P12DBubbleElement.h>
#include <stk_P1BubbleL2GMap.h>
#include <stk_P12DElement6pt.h>
#include <stk_SeqPairedL2GMap.h>
#include <Assembler.h>
#include <MeshUtils.h>
#include <PlottingUtils.h>
#include <PetscData.h>
#include <map>

// Create an equilateral mesh
void GetEquilateralMesh(CoordConn& MD, const int nDiv);

// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD, const LocalToGlobalMap& L2GMap, 
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// Exact solution
void GetExactSolution(const double* X, double* UP);

// L2 error in velocities and pressure
void ComputeL2Error(const std::vector<Element*>& UElmArray,
		    const std::vector<Element*>& PElmArray,
		    const LocalToGlobalMap& L2GMap,
		    const double* sol,
		    double* err);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a mesh equilateral triangles
  CoordConn MD;
  GetEquilateralMesh(MD, 3);
  PlotTecCoordConn((char*)"MD.tec", MD);

  // Create elements
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> UElmArray(MD.elements);
  std::vector<Element*> PElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const int* conn = &MD.connectivity[3*e];
      UElmArray[e] = new stk::P12DBubbleElement<2>(conn[0], conn[1], conn[2]);
      PElmArray[e] = new stk::P12DElement6pt<1>(conn[0], conn[1], conn[2]);
    }

  // Local to global map
  stk::P1BubbleL2GMap UL2GMap(UElmArray);
  StandardP12DMap PL2GMap(PElmArray);
  stk::SeqPairedL2GMap L2GMap(UL2GMap, PL2GMap);
  
  // Operations
  const double Mu = 1.;
  std::vector<stk::StokesOp*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    OpArray[e] = new stk::StokesOp(e, UElmArray[e], PElmArray[e], Mu, L2GMap);
  
  // Assembler
  StandardAssembler<stk::StokesOp> Asm(OpArray, L2GMap);
  
   // Dirichlet BCs
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, L2GMap, boundary, bvalues);

  const int nTotalDof = L2GMap.GetTotalNumDof();
  std::vector<double> Config(nTotalDof);
  std::fill(Config.begin(), Config.end(), 0.);
  const int nbcs = static_cast<int>(boundary.size());
  
  // PETSc data structures
  std::vector<int> nz;
  Asm.CountNonzeros(nz);
  PetscData PD;
  PD.Initialize(nz);
  MatSetOption(PD.stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);
  VecZeroEntries(PD.solutionVEC);
  
  // Set nonhomogeneous BCs
  for(int i=0; i<nbcs; ++i)
    Config[boundary[i]] = bvalues[i];
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  // Assemble stiffness (independent of configuration)
  Asm.Assemble(&Config[0], PD.resVEC, PD.stiffnessMAT);
      
  while(true)
    {
      // Assemble the residual
      Asm.Assemble(&Config[0], PD.resVEC);
      PD.SetDirichletBCs(boundary, bvalues);
      PD.Solve();
      if(PD.HasConverged(1.e-10, 1.,1.))
	{ break; }
      VecScale(PD.solutionVEC, -1.);
      double* sol;
      VecGetArray(PD.solutionVEC, &sol);
      for(int i=0; i<nTotalDof; ++i)
	Config[i] += sol[i];
      VecRestoreArray(PD.solutionVEC, &sol);
    }
      
  // L2 norm of errors
  double err[3];
  ComputeL2Error(UElmArray, PElmArray, L2GMap, &Config[0], err);
  std::cout<<"\nL2-norm of errors: "<<err[0]<<", "<<err[1]<<", "<<err[2]<<std::flush;
  
  // Plot
  std::vector<double> sol(3*MD.nodes);
  for(int e=0; e<MD.elements; ++e)
    for(int a=0; a<3; ++a)
      { const int n = MD.connectivity[3*e+a]-1;
	for(int f=0; f<3; ++f)
	  sol[3*n+f] = Config[L2GMap.Map(f,a,e)]; }
  
  PlotTecCoordConnWithNodalFields((char*)"sol.tec", MD, &sol[0], 3);

  // Exact solution
  for(int e=0; e<MD.elements; ++e)
    for(int a=0; a<3; ++a)
      { const int n = MD.connectivity[3*e+a]-1;
	const double* X = &MD.coordinates[2*n];
	double UP[3];
	GetExactSolution(X, UP);
	for(int f=0; f<3; ++f)
	  sol[3*n+f] = UP[f]; }
  PlotTecCoordConnWithNodalFields((char*)"exsol.tec", MD, &sol[0], 3);
  
  // Clean up
  PD.Destroy();
  for(auto& x:UElmArray) delete x;
  for(auto& x:PElmArray) delete x;
  for(auto& x:OpArray) delete x;
  
  // Finalize PETSc
  PetscFinalize();
}

// Create an equilateral mesh
void GetEquilateralMesh(CoordConn& MD, const int nDiv)
{
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  MD.nodes = 7;
  MD.elements = 6;
  MD.connectivity =
    std::vector<int>({1,2,3, 1,3,4, 1,4,5, 1,5,6, 1,6,7, 1,7,2});
  MD.coordinates.clear();
  const double PI = 4.*std::atan(1.);
  MD.coordinates.push_back( 0. );
  MD.coordinates.push_back( 0. );
  for(int i=0; i<6; ++i)
    { double theta = 2.*PI*static_cast<double>(i)/6.;
      MD.coordinates.push_back( std::cos(theta) );
      MD.coordinates.push_back( std::sin(theta) ); }

  // Subdivide
  for(int i=0; i<nDiv; ++i)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);

  return;
}


// Exact velocity+pressure solution
void GetExactSolution(const double* X, double* UP)
{
  const double& x = X[0];
  const double& y = X[1];

  //velocity
  UP[0] = std::exp(x)*((x-1.)*std::cos(y) + y*std::sin(y))/2.;
  UP[1] = std::exp(x)*(y*std::cos(y) - (x+1.)*std::sin(y))/2.;

  //pressure
  UP[2] = 2.*std::exp(x)*std::cos(y);

  return;
}


// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  std::set<int> bdofs({});
  std::vector<std::vector<int>> ElmNbs;
  GetCoordConnFaceNeighborList(MD, ElmNbs);
  double UP[6];
  int locnodes[2];
  const int nfacedofs = 2;
  
  std::map<int, double> bdofs2bvalues({});
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(ElmNbs[e][2*f]<0)
	{
	  // Nodes on this face
	  const int n0 = MD.connectivity[3*e+f]-1;
	  const int n1 = MD.connectivity[3*e+(f+1)%3]-1;
	  const double* X0 = &MD.coordinates[2*n0];
	  const double* X1 = &MD.coordinates[2*n1];
	  
	  // Solutions at the two nodes
	  GetExactSolution(X0, UP);
	  GetExactSolution(X1, UP+3);
	  
	  // Local dof numbers
	  locnodes[0] = f; 
	  locnodes[1] = (f+1)%3;
	  
	  for(int field=0; field<2; ++field)
	    for(int i=0; i<nfacedofs; ++i)
	      bdofs2bvalues[L2GMap.Map(field, locnodes[i], e)] = UP[3*i+field];
	}
  
  // Fix the pressure at some point
  { int e = MD.elements/2;
    int a = 0;
    int n = MD.connectivity[3*e+a]-1;
    const double* X = &MD.coordinates[2*n];
    double UP[3];
    GetExactSolution(X, UP);
    bdofs2bvalues[L2GMap.Map(2,a,e)] = UP[2]; }
  
  boundary.clear();
  bvalues.clear();
  for(auto& it:bdofs2bvalues)
    { boundary.push_back( it.first );
      bvalues.push_back( it.second ); }

  return;
}


// L2 error in velocities and pressure
void ComputeL2Error(const std::vector<Element*>& UElmArray,
		    const std::vector<Element*>& PElmArray,
		    const LocalToGlobalMap& L2GMap,
		    const double* sol,
		    double* err)
{
  // Initialize errors to 0
  err[0] = err[1] = err[2] = 0.;

  double UP[3];
  double FE[3];
  std::vector<int> ndofs(3);
  const int nElements = static_cast<int>(UElmArray.size());
  for(int e=0; e<nElements; ++e)
    {
      const auto* UElm = UElmArray[e];
      const auto* PElm = PElmArray[e];
      const auto& Qwts = UElm->GetIntegrationWeights(0);
      const auto& Qpts = UElm->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      ndofs[0] = UElm->GetDof(0); assert(ndofs[0]==4);
      ndofs[1] = UElm->GetDof(1); assert(ndofs[1]==4); 
      ndofs[2] = PElm->GetDof(0); assert(ndofs[2]==3);

      // Integrate over this element
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  const double* X = &Qpts[2*q];
	  GetExactSolution(X, UP);
	  
	  // FE solution here
	  FE[0] = FE[1] = FE[2] = 0.;
	  for(int f=0; f<2; ++f)
	    for(int a=0; a<ndofs[f]; ++a)
	      FE[f] += sol[L2GMap.Map(f,a,e)]*UElm->GetShape(f,q,a);
	  for(int a=0; a<ndofs[2]; ++a)
	    FE[2] += sol[L2GMap.Map(2,a,e)]*PElm->GetShape(0,q,a);
	  
	  // Error
	  for(int f=0; f<3; ++f)
	    err[f] += Qwts[q]*(FE[f]-UP[f])*(FE[f]-UP[f]);
	}
    }
  for(int f=0; f<3; ++f)
    err[f] = std::sqrt(err[f]);

  return;
}
