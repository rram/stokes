// Sriramajayam

#include <stk_StabilizedStokesOperation.h>
#include <P13DElement.h>
#include <Assembler.h>
#include <MeshUtils.h>
#include <PlottingUtils.h>
#include <PetscData.h>
#include <map>

// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD, const LocalToGlobalMap& L2GMap, 
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// Exact solution
void GetExactSolution(const double* X, double* UP);

// L2 error in velocities and pressure
void ComputeL2Error(const std::vector<Element*>& ElmArray,
		    const LocalToGlobalMap& L2GMap,
		    const double* sol,
		    double* err);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Read the mesh over a cube
  CoordConn MD;
  MD.nodes_element = 4;
  MD.spatial_dimension = 3;
  ReadTecplotFile((char*)"cube.msh", MD);
  double hVal = 1./3.;
  for(int i=0; i<0; ++i)
    { SubdivideTetrahedra(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      hVal /= 2.; } 
  PlotTecCoordConn((char*)"MD.tec", MD);
 
  // Create elements
  Segment<3>::SetGlobalCoordinatesArray(MD.coordinates);
  Triangle<3>::SetGlobalCoordinatesArray(MD.coordinates);
  Tetrahedron::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    { const int* conn = &MD.connectivity[4*e];
      ElmArray[e] = new P13DElement<4>(conn[0], conn[1], conn[2], conn[3]); }

  // Local to global map
  StandardP13DMap L2GMap(ElmArray);

  // Create operations
  const double Mu = 1.;
  std::vector<stk::StabStokesOp*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    OpArray[e] = new stk::StabStokesOp(e, ElmArray[e], Mu, hVal, L2GMap);

  // Assembler
  StandardAssembler<stk::StabStokesOp> Asm(OpArray, L2GMap);

  // State variables
  const int nTotalDofs = L2GMap.GetTotalNumDof();
  std::vector<double> state(nTotalDofs);
  state.resize(nTotalDofs);
  std::fill(state.begin(), state.end(), 0.);

  // Dirichlet boundary conditions
  std::vector<double> bvalues;
  std::vector<int> boundary;
  GetDirichletBCs(MD, L2GMap, boundary, bvalues);
  const int nbcs = static_cast<int>(boundary.size());
  
  // Sparse data structures
  PetscData PD;
  std::vector<int> nnz;
  Asm.CountNonzeros(nnz);
  PD.Initialize(nnz);

  // Set nonhomogeneous bcs
  for(int i=0; i<nbcs; ++i)
    state[boundary[i]] = bvalues[i];
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  // Assemble the stiffness, which is state-independent
  Asm.Assemble(&state[0], PD.resVEC, PD.stiffnessMAT);
  
  // Solve
  while(true)
    {
      // Set bcs
      PD.SetDirichletBCs(boundary, bvalues);
      
      // Solve
      PD.Solve();
      
      // Update the configuration
      VecScale(PD.solutionVEC, -1.);
      double* sol;
      VecGetArray(PD.solutionVEC, &sol);
	for(int i=0; i<nTotalDofs; ++i)
	  state[i] += sol[i];
      VecRestoreArray(PD.solutionVEC, &sol);
      
      // Check convergence
      if(PD.HasConverged(1.e-8, 1., 1.))
	break;
      
      // Assemble the residual at the update configuration
      Asm.Assemble(&state[0], PD.resVEC);
    }

  // Plot
  PlotTecCoordConnWithNodalFields((char*)"sol.tec", MD, &state[0], 4);

   // L2 norm of errors
  double err[4];
  ComputeL2Error(ElmArray, L2GMap, &state[0], err);
  std::cout<<"\nL2-norm of errors: "
	   <<err[0]<<", "<<err[1]<<", "<<err[2]<<", "<<err[3]<<std::flush;
  
  // Exact solution
  std::vector<double> sol(4*MD.nodes);
  for(int e=0; e<MD.elements; ++e)
    for(int a=0; a<4; ++a)
      { const int n = MD.connectivity[4*e+a]-1;
	const double* X = &MD.coordinates[3*n];
	double UP[4];
	GetExactSolution(X, UP);
	for(int f=0; f<4; ++f)
	  sol[4*n+f] = UP[f]; }
  PlotTecCoordConnWithNodalFields((char*)"exsol.tec", MD, &sol[0], 4);
  
  
  // Clean up
  PD.Destroy();
  for(auto& x:ElmArray) delete x;
  for(auto& x:OpArray) delete x;
  
  // Finalize PETSc
  PetscFinalize();
}
   


// Exact velocity+pressure solution
void GetExactSolution(const double* X, double* UP)
{
  const double& x = X[0];
  const double& y = X[1];

  // velocity
  UP[0] = std::exp(x)*((x-1.)*std::cos(y) + y*std::sin(y))/2.;
  UP[1] = std::exp(x)*(y*std::cos(y) - (x+1.)*std::sin(y))/2.;
  UP[2] = 0.;

  // pressure
  UP[3] = 2.*std::exp(x)*std::cos(y);

  return;
}


// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  std::set<int> bdofs({});
  std::vector<std::vector<int>> ElmNbs;
  GetCoordConnFaceNeighborList(MD, ElmNbs);
  double UP[4];

  // enumeration of dofs on each face
  const int nfaces = 4;
  const int facedofs[] = {2,1,0,
			  2,0,3,
			  2,3,1,
			  0,1,3};
  
  std::map<int, double> bdofs2bvalues({});
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<nfaces; ++f)
      if(ElmNbs[e][2*f]<0)
	for(int i=0; i<3; ++i)
	  {
	    const int a = facedofs[3*f+i];
	    const int n = MD.connectivity[4*e+a]-1;
	    const double* X = &MD.coordinates[3*n];
	    GetExactSolution(X, UP);
	    for(int field=0; field<3; ++field)
	      bdofs2bvalues[L2GMap.Map(field, a, e)] = UP[field];
	  }

  // Fix pressure at one point
  { int e = MD.elements/2;
    int a = 0;
    const int n = MD.connectivity[4*e+a]-1;
    const double* X = &MD.coordinates[3*n];
    GetExactSolution(X, UP);
    bdofs2bvalues[L2GMap.Map(3,a,e)] = UP[3]; }
  
  boundary.clear();
  bvalues.clear();
  for(auto& it:bdofs2bvalues)
    { boundary.push_back( it.first );
      bvalues.push_back( it.second ); }

  return;
}


// L2 error in velocities and pressure
void ComputeL2Error(const std::vector<Element*>& ElmArray,
		    const LocalToGlobalMap& L2GMap,
		    const double* sol,
		    double* err)
{
  // Initialize errors to 0
  err[0] = err[1] = err[2] = err[3] = 0.;

  double UP[4];
  double FE[4];
  const int nElements = static_cast<int>(ElmArray.size());
  for(int e=0; e<nElements; ++e)
    {
      const auto* Elm = ElmArray[e];
      const auto& Qwts = Elm->GetIntegrationWeights(0);
      const auto& Qpts = Elm->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const int ndofs = Elm->GetDof(0); assert(ndofs==4);
      
      // Integrate over this element
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  const double* X = &Qpts[3*q];
	  GetExactSolution(X, UP);
	  
	  // FE solution here
	  FE[0] = FE[1] = FE[2] = FE[3] = 0.;
	  for(int f=0; f<4; ++f)
	    for(int a=0; a<ndofs; ++a)
	      FE[f] += sol[L2GMap.Map(f,a,e)]*Elm->GetShape(f,q,a);
	  
	  // Error
	  for(int f=0; f<4; ++f)
	    err[f] += Qwts[q]*(FE[f]-UP[f])*(FE[f]-UP[f]);
	}
    }
  for(int f=0; f<4; ++f)
    err[f] = std::sqrt(err[f]);
  
  return;
}
