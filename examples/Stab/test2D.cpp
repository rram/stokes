// Sriramajayam

#include <stk_StabilizedStokesOperation.h>
#include <P12DElement.h>
#include <Assembler.h>
#include <MeshUtils.h>
#include <PlottingUtils.h>
#include <PetscData.h>
#include <map>

// Create a square mesh
double GetSquareMesh(CoordConn& MD, const int nDiv);

// Create an equilateral mesh
double GetEquilateralMesh(CoordConn& MD, const int nDiv);

// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD, const LocalToGlobalMap& L2GMap, 
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// Exact solution
void GetExactSolution(const double* X, double* UP);

// L2 error in velocities and pressure
void ComputeL2Error(const std::vector<Element*>& ElmArray,
		    const LocalToGlobalMap& L2GMap,
		    const double* sol,
		    double* err);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Create a mesh equilateral triangles
  CoordConn MD;
  //const double meshsize = GetSquareMesh(MD, 4);
  const double meshsize = GetEquilateralMesh(MD, 3);
  std::cout<<"\nMesh size: "<<meshsize<<std::flush;
  PlotTecCoordConn((char*)"MD.tec", MD);

  // Create elements
  Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    {
      const int* conn = &MD.connectivity[3*e];
      ElmArray[e] = new P12DElement<3>(conn[0], conn[1], conn[2]);
    }

  // Local to global map
  StandardP12DMap L2GMap(ElmArray);
  
  // Operations
  const double Mu = 1.;
  std::vector<stk::StabStokesOp*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; ++e)
    OpArray[e] = new stk::StabStokesOp(e, ElmArray[e], Mu, meshsize, L2GMap);
  
  // Assembler
  StandardAssembler<stk::StabStokesOp> Asm(OpArray, L2GMap);
  
  // Dirichlet BCs
  std::vector<int> boundary;
  std::vector<double> bvalues;
  GetDirichletBCs(MD, L2GMap, boundary, bvalues);

  const int nTotalDof = L2GMap.GetTotalNumDof();
  std::vector<double> Config(nTotalDof);
  std::fill(Config.begin(), Config.end(), 0.);
  const int nbcs = static_cast<int>(boundary.size());
  
  // PETSc data structures
  std::vector<int> nz;
  Asm.CountNonzeros(nz);
  PetscData PD;
  PD.Initialize(nz);
  VecZeroEntries(PD.solutionVEC);
  
  // Set nonhomogeneous BCs
  for(int i=0; i<nbcs; ++i)
    Config[boundary[i]] = bvalues[i];
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  // Assemble stiffness (independent of configuration)
  Asm.Assemble(&Config[0], PD.resVEC, PD.stiffnessMAT);
      
  while(true)
    {
      // Assemble the residual
      Asm.Assemble(&Config[0], PD.resVEC);
      PD.SetDirichletBCs(boundary, bvalues);
      PD.Solve();
      if(PD.HasConverged(1.e-10, 1.,1.))
	{ break; }
      VecScale(PD.solutionVEC, -1.);
      double* sol;
      VecGetArray(PD.solutionVEC, &sol);
      for(int i=0; i<nTotalDof; ++i)
	Config[i] += sol[i];
      VecRestoreArray(PD.solutionVEC, &sol);
    }

  // L2 norm of errors
  double err[3];
  ComputeL2Error(ElmArray, L2GMap, &Config[0], err);
  std::cout<<"\nL2-norm of errors: "
	   <<err[0]<<", "<<err[1]<<", "<<err[2]<<std::flush;
  
  // Plot
  PlotTecCoordConnWithNodalFields((char*)"sol.tec", MD, &Config[0], 3);

  // Exact solution
  std::vector<double> sol(3*MD.nodes);
  for(int e=0; e<MD.elements; ++e)
    for(int a=0; a<3; ++a)
      { const int n = MD.connectivity[3*e+a]-1;
	const double* X = &MD.coordinates[2*n];
	double UP[3];
	GetExactSolution(X, UP);
	for(int f=0; f<3; ++f)
	  sol[3*n+f] = UP[f]; }
  PlotTecCoordConnWithNodalFields((char*)"exsol.tec", MD, &sol[0], 3);
  
  // Clean up
  PD.Destroy();
  for(auto& x:ElmArray) delete x;
  for(auto& x:OpArray) delete x;
  
  // Finalize PETSc
  PetscFinalize();
}




// Create a mesh of triangles over a square
double GetSquareMesh(CoordConn& MD, const int nDiv)
{
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  MD.nodes = 5;
  MD.elements = 4;
  {
    int conn[] = {1,2,5, 2,3,5, 3,4,5, 4,1,5};
    MD.connectivity.assign(conn, conn+12);
    double coord[] = {-1.,-1., 1.,-1., 1.,1., -1.,1., 0.,0.};
    MD.coordinates.assign(coord, coord+10);
  }
  double meshsize = 2.;
  for(int i=0; i<nDiv; i++)
    { SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      meshsize /= 2.; }
  
  return meshsize;
}

// Create an equilateral mesh
double GetEquilateralMesh(CoordConn& MD, const int nDiv)
{
  MD.nodes_element = 3;
  MD.spatial_dimension = 2;
  MD.nodes = 7;
  MD.elements = 6;
  MD.connectivity =
    std::vector<int>({1,2,3, 1,3,4, 1,4,5, 1,5,6, 1,6,7, 1,7,2});
  MD.coordinates.clear();
  const double PI = 4.*std::atan(1.);
  MD.coordinates.push_back( 0. );
  MD.coordinates.push_back( 0. );
  for(int i=0; i<6; ++i)
    { double theta = 2.*PI*static_cast<double>(i)/6.;
      MD.coordinates.push_back( std::cos(theta) );
      MD.coordinates.push_back( std::sin(theta) ); }
  double meshsize = 1.;
  
  // Subdivide
  for(int i=0; i<nDiv; ++i)
    { SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
      meshsize /= 2.; }

  return meshsize;
}


// Exact velocity+pressure solution
void GetExactSolution(const double* X, double* UP)
{
  const double& x = X[0];
  const double& y = X[1];

  //velocity & pressure
  UP[0] = std::exp(x)*((x-1.)*std::cos(y) + y*std::sin(y))/2.;
  UP[1] = std::exp(x)*(y*std::cos(y) - (x+1.)*std::sin(y))/2.;
  UP[2] = 2.*std::exp(x)*std::cos(y);
  
  // velocity & pressure
  //UP[0] = 20.*x*std::pow(y,4.) - 4.*std::pow(x,5.);
  //UP[1] = 20.*y*std::pow(x,4.) - 4.*std::pow(y,5.);
  //UP[2] = -(120.*x*x*y*y -20.*std::pow(x,4.) - 20.*std::pow(y,4.));

  return;
}


// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  std::set<int> bdofs({});
  std::vector<std::vector<int>> ElmNbs;
  GetCoordConnFaceNeighborList(MD, ElmNbs);
  double UP[3];
  
  std::map<int, double> bdofs2bvalues({});
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<3; ++f)
      if(ElmNbs[e][2*f]<0)
	for(int i=0; i<2; ++i)
	  {
	    const int a = (f+i)%3;
	    const int n = MD.connectivity[3*e+a]-1;
	    const double* X = &MD.coordinates[2*n];
	    GetExactSolution(X, UP);
	    for(int field=0; field<2; ++field)
	      bdofs2bvalues[L2GMap.Map(field, a, e)] = UP[field];
	  }

  // Fix the pressure at one node
  {
    int e = MD.elements/2;
    int a = 0;
    int n = MD.connectivity[3*e+a]-1;
    const double* X = &MD.coordinates[2*n];
    GetExactSolution(X, UP);
    bdofs2bvalues[L2GMap.Map(2,a,e)] = UP[2];
  }
  
  boundary.clear();
  bvalues.clear();
  for(auto& it:bdofs2bvalues)
    { boundary.push_back( it.first );
      bvalues.push_back( it.second ); }

  return;
}


// L2 error in velocities and pressure
void ComputeL2Error(const std::vector<Element*>& ElmArray,
		    const LocalToGlobalMap& L2GMap,
		    const double* sol,
		    double* err)
{
  // Initialize errors to 0
  err[0] = err[1] = err[2] = 0.;

  double UP[3];
  double FE[3];
  const int nElements = static_cast<int>(ElmArray.size());
  for(int e=0; e<nElements; ++e)
    {
      const auto* Elm = ElmArray[e];
      const auto& Qwts = Elm->GetIntegrationWeights(0);
      const auto& Qpts = Elm->GetIntegrationPointCoordinates(0);
      const int nQuad = static_cast<int>(Qwts.size());
      const int ndofs = Elm->GetDof(0); assert(ndofs==3);
      
      // Integrate over this element
      for(int q=0; q<nQuad; ++q)
	{
	  // Exact solution here
	  const double* X = &Qpts[2*q];
	  GetExactSolution(X, UP);
	  
	  // FE solution here
	  FE[0] = FE[1] = FE[2] = 0.;
	  for(int f=0; f<3; ++f)
	    for(int a=0; a<ndofs; ++a)
	      FE[f] += sol[L2GMap.Map(f,a,e)]*Elm->GetShape(f,q,a);
	  
	  // Error
	  for(int f=0; f<3; ++f)
	    err[f] += Qwts[q]*(FE[f]-UP[f])*(FE[f]-UP[f]);
	}
    }
  for(int f=0; f<3; ++f)
    err[f] = std::sqrt(err[f]);

  return;
}
