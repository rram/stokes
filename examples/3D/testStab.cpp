// Sriramajayam

#include <stk_StabilizedStokesProblem.h>
#include <geom_LevelSetManifold.h>
#include <msh_StdTetMesh.h>
#include <PlottingUtils.h>
#include <MeshUtils.h>
#include <map>
#include <msh_ReadTecplotFile.h>

// Level set function for a sphere
void SphereFunc(const double* X, double& F,
		double* dF, double* d2F, void* params);

// Dirichlet BCs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

// exact solution
void GetExactSolution(const double* X, double* UP);

int main(int argc, char** argv)
{
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);

  // Read mesh over a cube
  std::vector<double> coordinates;
  std::vector<int> connectivity;
  msh::ReadTecplotFile("AcuteCubeTet.msh", 3, coordinates, connectivity);
  const double meshsize = 0.2;
  msh::SeqCoordinates Coord(3, coordinates);
  msh::StdTetConnectivity Conn(connectivity);
  msh::StdTetMesh BG(Coord, Conn);
  msh::PlotTecStdTetMesh("BG.tec", BG);

  // Create geometry of a sphere
  geom::NLSolverParams nlparams({.ftol=1.e-6, .ttol=1.e-6, .max_iter=25});
  geom::LevelSetFunction LSFunc = SphereFunc;
  geom::LevelSetManifold<3> Geom(nlparams, LSFunc);

  // Meshing options
  stk::Meshing_Options mesh_options({.nMaxThreads=1,
	.MaxVertValency=40, .MaxElmValency=40, .nSteps=4, .nIters=8, .nBdSamples=20});

  // Viscosity
  const double mu = 1.;

  // Stabilized stokes problem
  stk::StabStokesProblem SP(mu, meshsize, BG, Geom, mesh_options);

  // Iterate
  for(int iter=0; iter<1; ++iter)
    {
      std::cout<<"\nIteration "<<iter<<std::flush;

      // Setup iteration
      SP.SetupIteration();
      const auto& MD = SP.GetConformingMesh();
      PlotTecCoordConn((char*)"MD.tec", MD);

      // Boundary conditions
      const auto& L2GMap = SP.GetLocalToGlobalMap();
      stk::BCParams bcs;
      GetDirichletBCs(MD, L2GMap, bcs.dirichlet_dofs, bcs.dirichlet_values);

      // Solve
      SP.Compute(bcs);

      // Plot the state
      std::string filename = "sol-"+std::to_string(iter)+".tec";
      SP.PlotState(filename);

      // Compute the net force and moment
      double Frc[3], Moment[3];
      double origin[] = {0.,0.,0.};
      SP.ComputeNetForceAndMoment(Frc, origin, Moment);
      std::cout<<"\nForce = "<<Frc[0]<<", "<<Frc[1]<<", "<<Frc[2]
	       <<"\nMoment = "<<Moment[0]<<", "<<Moment[1]<<", "<<Moment[2]
	       <<std::flush;

      // Finalize this iteration
      SP.FinalizeIteration();
    }

  // Finalize PETSc
  PetscFinalize();
}

// Level set function for a sphere
void SphereFunc(const double* X, double& F,
		double* dF, double* d2F, void* params)
{
  const double rad2 = 0.4*0.4;
  double r2 = X[0]*X[0]+X[1]*X[1]+X[2]*X[2];
  F = r2-rad2;
  if(dF!=nullptr)
    { dF[0] = 2.*X[0]; dF[1] = 2.*X[1]; dF[2] = 2.*X[2]; }
  if(d2F!=nullptr)
    { d2F[0] = 2.; d2F[1] = 0.; d2F[2] = 0.;
      d2F[3] = 0.; d2F[4] = 2.; d2F[5] = 0.;
      d2F[6] = 0.; d2F[7] = 0.; d2F[8] = 2.; }

  return;
}

// exact solution
void GetExactSolution(const double* X, double* UP)
{
  const double& x = X[0];
  const double& y = X[1];
  
  //velocity
  UP[0] = std::exp(x)*((x-1.)*std::cos(y) + y*std::sin(y))/2.;
  UP[1] = std::exp(x)*(y*std::cos(y) - (x+1.)*std::sin(y))/2.;
  UP[2] = 0.;
  
  //pressure
  UP[3] = 2.*std::exp(x)*std::cos(y);

  return;
}


// Dirichlet bcs
void GetDirichletBCs(const CoordConn& MD,
		     const LocalToGlobalMap& L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues)
{
  std::vector<std::vector<int>> ElmNbs;
  GetCoordConnFaceNeighborList(MD, ElmNbs);
  double UP[4];

  // enumeration of dofs on each face
  const int nfaces = 4;
  const int facedofs[] = {2,1,0,
			  2,0,3,
			  2,3,1,
			  0,1,3};
  
  std::map<int, double> bdofs2bvalues({});
  for(int e=0; e<MD.elements; ++e)
    for(int f=0; f<nfaces; ++f)
      if(ElmNbs[e][2*f]<0)
	for(int i=0; i<3; ++i)
	  {
	    const int a = facedofs[3*f+i];
	    const int n = MD.connectivity[4*e+a]-1;
	    const double* X = &MD.coordinates[3*n];
	    GetExactSolution(X, UP);
	    for(int field=0; field<3; ++field)
	      bdofs2bvalues[L2GMap.Map(field, a, e)] = UP[field];
	  }

  // Fix pressure at one point
  { int e = MD.elements/2;
    int a = 0;
    const int n = MD.connectivity[4*e+a]-1;
    const double* X = &MD.coordinates[3*n];
    GetExactSolution(X, UP);
    bdofs2bvalues[L2GMap.Map(3,a,e)] = UP[3]; }
  
  boundary.clear();
  bvalues.clear();
  for(auto& it:bdofs2bvalues)
    { boundary.push_back( it.first );
      bvalues.push_back( it.second ); }

  return;
}

