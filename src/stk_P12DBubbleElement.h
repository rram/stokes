
#ifndef STK_P12D_BUBBLE_ELEMENT_H
#define STK_P12D_BUBBLE_ELEMENT_H

#include <stk_P12DElement6pt.h>
#include <stk_P1BubbleShape.h>
#include <TriangleQuadratures.h>

namespace stk
{
  //! Fields are ordered as Ux, Uy
  //! Ux, Uy are interpolated by P1BubbleShape functions
  template<int NFields>
    class P12DBubbleElement: public Element
    {
    public:
      //! Constructor
      //! \param[in] i1, i2, i3 Node numbers
    P12DBubbleElement(int i1, int i2, int i3)
      :Element()
	{
	  // Create the element geometry
	  TriGeom = new Triangle<2>(i1, i2, i3);
	
	  // interpolations
	  P1BubbleShape<2> Shp;
	  ShapesEvaluated ShpEval(Triangle_6pt::Bulk, &Shp, TriGeom);
	  AddBasisFunctions(ShpEval);
	  for(int i=0; i<NFields; ++i)
	    AppendField(i);
	}


      //! Destructor
      virtual ~P12DBubbleElement() { delete TriGeom; }

      //! Copy constructor
      //! \param[in] obj Object to be copied
    P12DBubbleElement(const P12DBubbleElement<NFields>& elm)
      :Element(elm)
      { TriGeom = new Triangle<2>(*(elm.TriGeom)); }
    
      //! Cloning
      virtual P12DBubbleElement<NFields>* Clone() const override
      { return new P12DBubbleElement(*this); }

      //! Returns the element geometry
      virtual const Triangle<2>& GetElementGeometry() const override
      { return *TriGeom; }

    protected:
      //! Mapping from field to its index in this element
      //! \param[in] field Global field number
      //! \return Index of given field
      virtual int GetFieldIndex(int field) const override
      { return 0; }

    private:
      Triangle<2>* TriGeom; //! Element geometry
    };
  
}

#endif
