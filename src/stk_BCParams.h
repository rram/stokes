// Sriramajayam

#ifndef STK_BC_PARAMS_H
#define STK_BC_PARAMS_H

namespace stk
{
  //! Helper struct for passing Dirichlet bcs
  struct BCParams
  {
    std::vector<int> dirichlet_dofs;
    std::vector<double> dirichlet_values;
  };
}


#endif
