// Sriramajayam

#ifndef STK_STOKES_PROBLEM_2D_H
#define STK_STOKES_PROBLEM_2D_H

#include <stk_MeshUtils.h>
#include <stk_StokesOperation.h>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <Assembler.h>
#include <geom_ImplicitManifold.h>
#include <External.h>
#include <PetscData.h>
#include <stk_BCParams.h>

namespace stk
{  
  class StokesProblem
  {
  public:
    //! Constructor
    //! \param[in] mu Viscosity
    //! \param[in] bg Background mesh to be used
    //! \param[in] gm Geometry, assume implicit manifold to avoid closest point calcs
    //! \param[in] mesh_opts Meshing options
    StokesProblem(const double mu,
		  msh::StdOrphanMesh& bg,
		  geom::ImplicitManifold& gm,
		  const Meshing_Options& mesh_opts);

    
    //! Destructor
    virtual ~StokesProblem();

    //! Disable copy and assignment
    StokesProblem(const StokesProblem&) = delete;
    StokesProblem& operator=(const StokesProblem&) = delete;

    //! Helper method to set up a run
    //! Set up the mesh, elements, operartions, assembler, sparse data structures
    void SetupIteration();

    //! Main functionality: compute velocity and pressure
    //! \param[in] bc Dirichlet bcs
    void Compute(const BCParams& bc);

    //! Helper method to finalize an iteration
    //! Deletes the mesh, elements, operations, assemblers and sparse data structures
    void FinalizeIteration();

    //! Get the viscosity
    double GetViscosity() const;
    
    //! Access the geometry
    const geom::ImplicitManifold& GetGeometry() const;

    //! Access the background mesh
    msh::StdOrphanMesh& GetBackgroundMesh();
    
    //! Access the conforming mesh
    const CoordConn& GetConformingMesh() const;

    //! Access the positive vertices
    const std::vector<int>& GetPositiveVertices() const;

    //! Access positively cut eleents and local nodes on the positive face
    const std::vector<int>& GetPositiveElmFacePairs() const;

    //! Access the solution
    const double* GetState() const;

    //! Utility to plot the state
    void PlotState(const std::string filename) const;

    //! Compute forces and moments on the boundary
    //! Moments are computed about the specified point
    void ComputeForcesAndMoments(double* F, const double* refX, double* M) const;
    
    //! Access the local to global map
    const LocalToGlobalMap& GetLocalToGlobalMap() const;

    //! Access the velocity element array
    const std::vector<Element*>& GetVelocityElementArray() const;

    //! Access the pressure element array
    const std::vector<Element*>& GetPressureElementArray() const;
    
    //! Access the meshing options
    const Meshing_Options& GetMeshingOptions() const;

  private:

    //! Helper function to setup elements, operations & sparse data structures
    void Setup2D();

    //! Helper function to compute forces and moments in 2D
    //! Moments are computed about the specified point
    void ComputeForcesAndMoments2D(double* F, const double* refX, double* M) const;
    
    const double Mu; //!< Viscosity
    msh::StdOrphanMesh& BG; //!< Reference to the background mesh
    geom::ImplicitManifold& Geom; //!< Reference to the geometry
    const Meshing_Options mesh_options; //! Options to create a conforming mesh
    const int SPD; //!< Spatial dimension
    
    CoordConn MD; //!< Conforming mesh
    std::vector<int> PosVerts; //!< Positive vertics in the conforming mesh
    std::vector<int> PosElmsNodes; //!< Positive elements and local nodes on the positive face

    std::vector<Element*> UElmArray; //!< Velocity elements
    std::vector<Element*> PElmArray; //!< Pressure elements
    LocalToGlobalMap* UL2GMap; //!< Local to global map for velocities
    LocalToGlobalMap* PL2GMap; //!< Local to global map for pressure field
    LocalToGlobalMap* L2GMap; //!< Combined local to global map
    std::vector<stk::StokesOp*> OpArray; //!< Operations array
    StandardAssembler<stk::StokesOp>* Asm; //!< Assembler
    std::vector<double> state; //!< State solution
    
    PetscData PD; //!< Petsc data structures
    IS uindxSet, pindxSet;

    mutable bool is_iteration_setup;
    mutable bool is_iteration_finalized;
    mutable bool is_state_computed;
  };
  
}



#endif
