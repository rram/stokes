
#include <stk_MeshUtils.h>
#include <um_TetrahedronModule>
#include <map>
#include <stk_MeshUtils_impl.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace stk
{
  // Anonymous namespace
  namespace
  {
    // Aliases
    using BGType = msh::StdOrphanMesh;
    using GeomType = geom::ImplicitManifold;
    using WSType = um::SDWorkspace<BGType, GeomType>;
    using MshrType = um::DomainTriangulator<BGType, GeomType>;
    using WMType = um::WorkingMesh<BGType>;
    using OneRingType = dvr::OneRingData<WMType>;
  }

  // Interface for computing a mesh conforming to a given implicit manifold
  template<>
  void GetConformingMesh<3>(msh::StdOrphanMesh& BG,
			    geom::ImplicitManifold& Geom,
			    const Meshing_Options& options,
			    std::vector<double>& coordinates,
			    std::vector<int>& connectivity,
			    std::vector<int>& posverts,
			    std::vector<int>& poselmsnodes)
  {
    // Set the max number of threads
    omp_set_num_threads(options.nMaxThreads);

    const int nFacesPerElm = BG. GetNumFacesPerElement();
    const int nNodesPerFace = BG.GetNumNodesPerFace();
    
    // Create a workspace for the background mesh
    WSType BGws(BG, Geom);

    // Evaluate the implicit function at all nodes
    const int nbgnodes = BG.GetNumNodes();
#pragma omp parallel default(shared)
    {
      um::SignedDistance sd;
      std::map<int, um::SignedDistance> sdvals({});
#pragma omp for
      for(int i=0; i<nbgnodes; ++i)
	{
	  const double* X = BG.coordinates(i);
	  Geom.GetImplicitFunction(X, sd.value);
	  if(sd.value>0.) sd.sign = um::SDSignature::Plus;
	  else sd.sign = um::SDSignature::Minus;
	  sdvals.insert( std::make_pair(i,sd) );
	}

      // Merge one thread at a time
#pragma omp critical
      {
	for(auto& it:sdvals)
	  BGws.Set(it.first, it.second);
      }
    }

    // Create a domain triangulator
    MshrType Mshr(BGws);

    // Get the working mesh
    WMType& WM = Mshr.GetWorkingMesh();

    // Positive vertices
    const auto& PosVerts = Mshr.GetPositiveVertices();

    // Vertices to be relaxed
    std::vector<int> Ir({});

    // All nodes in the working mesh
    std::vector<int> wmnodes({});

    {
      // Boundary vertices of the background mesh should not be relaxed
      std::set<int> bg_bdnodes({});
      const int nbgelements = BG.GetNumElements();
      for(int e=0; e<nbgelements; ++e)
	{
	  const int* myconn = BG.connectivity(e);
	  const int* elmnbs = BG.GetElementNeighbors(e);
	  for(int f=0; f<nFacesPerElm; ++f)
	    if(elmnbs[2*f]<0)
	      {
		// This is a free face
		const int* locfacenodes = BG.GetLocalFaceNodes(f);
		for(int i=0; i<nNodesPerFace; ++i)
		  bg_bdnodes.insert(myconn[locfacenodes[i]]);
	      }
	}

      // Union of bg_bdnodes + positive vertices
      // should not be relaxed
      std::vector<int> fixednodes({});
      std::set_union(PosVerts.begin(), PosVerts.end(),
		     bg_bdnodes.begin(), bg_bdnodes.end(),
		     std::back_inserter(fixednodes));

      // All nodes of the working mesh
      WM.GetNodes(wmnodes);
      std::sort(wmnodes.begin(), wmnodes.end());

      // Vertices to relax
      std::set_difference(wmnodes.begin(), wmnodes.end(),
			  fixednodes.begin(), fixednodes.end(),
			  std::back_inserter(Ir));
    }
    
    // 1-ring data for the woring mesh
    dvr::ValencyHint vhint({
	.MaxVertValency=options.MaxVertValency,
	  .MaxElmValency=options.MaxElmValency});
    OneRingType RD(WM, wmnodes, vhint);
    
    // Quality metric
    dvr::GeomTet3DQuality<WMType> Quality(WM);

    // Max-min solver for relaxing vertices
    dvr::SolverSpecs solspecs({.nthreads=1, .nExtrema=2, .nIntersections=8});
    dvr::ReconstructiveMaxMinSolver<WMType> mmsolver(WM, RD, solspecs);

    // Vertex optimizer
    dvr::SeqMeshOptimizer Opt;

    // Relaxation direction generator
    dvr::RelaxationDirGenerator rdir_gen;
    rdir_gen.f_rdir = dvr::CartesianDirections<3>;

    // Project and relax in steps
    for(int step=1; step<=options.nSteps; ++step)
      {
	// Project boundary vertices in parallel
	omp_set_num_threads(options.nMaxThreads);
	const double alpha = static_cast<double>(step)/static_cast<double>(options.nSteps);
	Mshr.Project(alpha);

	// Relax inner vertices sequentially
	omp_set_num_threads(1);
	for(int iter=0; iter<options.nIters; ++iter)
	  { rdir_gen.params = &iter;
	    Mshr.Relax(Ir, Opt, Quality, mmsolver, rdir_gen); }
      }

    // Relaxation direction for boundary vertices
    if(options.nBdSamples>0)
      {
	um::MeshGeomPair<WMType, GeomType> MGPair(WM, Geom);
	dvr::RelaxationDirGenerator tgtdirgen;
	tgtdirgen.f_rdir = um::TangentDirection3D<decltype(MGPair)>;
	tgtdirgen.params = &MGPair;
	
	// Boundary projection function
	um::BoundaryProjector<GeomType> bdProj(Geom);
	
	// Solver for boundary vertex relaxation
	dvr::ProjMaxMinSampler<WMType>
	  BdSolver(WM, RD, options.nBdSamples, 1); // 1 thread
	
	// Relax positive vertices & 
	for(int iter=1; iter<=options.nIters; ++iter)
	  {
	    // Boundary relaxation
	    Mshr.Relax(PosVerts, Opt, Quality, BdSolver, tgtdirgen, &bdProj);
	    
	    // Interior relaxation
	    rdir_gen.params = &iter;
	    Mshr.Relax(Ir, Opt, Quality, mmsolver, rdir_gen);
	  }
      }
    
    // Positive element-facepairs
    const auto& PosElmFacePairs = Mshr.GetPositiveElmFacePairs();

    // Create an orphan mesh to return
    posverts.clear();
    poselmsnodes.clear();
    detail::Cast2OrphanMesh(WM, coordinates, connectivity,
			    PosVerts, posverts,
			    PosElmFacePairs, poselmsnodes);
    return;
  }
}
  
