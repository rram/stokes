// Sriramajayam

#include <stk_StokesOperation.h>
#include <cassert>
#include <iostream>
#include <cmath>

namespace stk
{
  // Constructor
  StokesOp::StokesOp(const int elmnum,
		     const Element* uelm, const Element* pelm,
		     const double mu, const LocalToGlobalMap& l2gmap)
    :DResidue(),
     ElmNum(elmnum),
     UElm(uelm),
     PElm(pelm),
     Mu(mu),
     L2GMap(l2gmap)
  {
    FieldsUsed.clear();
    const int nFields = L2GMap.GetNumFields(0);
    assert(nFields==3);
    assert((nFields==3 || nFields==4) && "stk::StokesOp- Unexpected number of fields");
    FieldsUsed.resize(nFields);
    for(int f=0; f<nFields; ++f)
      FieldsUsed[f] = f;

    // Check that both elements have the same quadrature rule
    assert( UElm->GetIntegrationWeights(0).size()==PElm->GetIntegrationWeights(0).size() &&
	    "stk::StokesOp- U and P elements have different quadrature rules");

    // Check assumptions on field numbering
    const int SPD = UElm->GetNumDerivatives(0);
    assert(SPD==2);
    assert(UElm->GetNumFields()==SPD && PElm->GetNumFields()==1);
    const auto& UFields = UElm->GetFields();
    for(int f=0; f<static_cast<int>(UElm->GetFields().size()); ++f)
      assert(UFields[f]==f);
    const auto& PFields = PElm->GetFields();
    assert(PFields[0]==0);
      
  }
  
  // Destructor
  StokesOp::~StokesOp() {}

  // Copy constructor
  StokesOp::StokesOp(const StokesOp& obj)
    :DResidue(obj),
     ElmNum(obj.ElmNum),
     UElm(obj.UElm),
     PElm(obj.PElm),
     Mu(obj.Mu),
     L2GMap(obj.L2GMap),
     FieldsUsed(obj.FieldsUsed) {}

  // Cloning
  StokesOp* StokesOp::Clone() const
  { return new StokesOp(*this); }
  
  // Returns the viscosity
  double StokesOp::GetViscosity() const
  { return Mu; }

  // Returns the fields used
  const std::vector<int>& StokesOp::GetField() const
  { return FieldsUsed; }

  // Returns the number of dofs for a field
  int StokesOp::GetFieldDof(const int field) const
  {
    if(field==static_cast<int>(FieldsUsed.size()-1))
      return PElm->GetDof(0);
    else
      return UElm->GetDof(field);
  }

  // Return the velocity element
  const Element* StokesOp::GetVelocityElement() const
  { return UElm; }
  
  // Return the pressure element
  const Element* StokesOp::GetPressureElement() const
  { return PElm; }

  // Returns the functional
  double StokesOp::GetEnergy(const void* state) const
  {
    // Get the state
    assert(state!=nullptr && "stk::StokesOp::GetEnergy()- state is not specified");
    const double* config = static_cast<const double*>(state);

    // Access the quadrature
    const auto& Qwts = UElm->GetIntegrationWeights(0);
    const int nQuad = static_cast<int>(Qwts.size());
    
    // Number of velocity dofs
    const int nUdofs = UElm->GetDof(0);
    
    // Number of pressure dofs
    const int nPdofs = PElm->GetDof(0);

    // Spatial dimension
    const int SPD = static_cast<int>(FieldsUsed.size())-1;

    // Velocity and pressure
    std::vector<double> U(SPD);
    double P;
    double divU;
    std::vector<std::vector<double>> dU(SPD, std::vector<double>(SPD));
    
    // Integrate the functional
    double Energy = 0.;
    for(int q=0; q<nQuad; ++q)
      {
	// Velocity and its gradient at this point
	for(int i=0; i<SPD; ++i)
	  { U[i] = 0.;
	    for(int j=0; j<SPD; ++j)
	      dU[i][j] = 0.; }
	for(int f=0; f<SPD; ++f)
	  for(int a=0; a<nUdofs; ++a)
	    {
	      const double& Ua = config[L2GMap.Map(f, a, ElmNum)];
	      U[f] += Ua*UElm->GetShape(f, q, a);
	      for(int j=0; j<SPD; ++j)
		dU[f][j] += Ua*UElm->GetDShape(f, q, a, j);
	    }
	
	// Divergence of U
	divU = 0.;
	for(int i=0; i<SPD; ++i)
	  divU += dU[i][i];

	// Pressure
	P = 0.;
	for(int a=0; a<nPdofs; ++a)
	  P += config[L2GMap.Map(SPD, a, ElmNum)]*PElm->GetShape(0, q, a);

	// Update the energy
	for(int i=0; i<SPD; ++i)
	  for(int j=0; j<SPD; ++j)
	    Energy += Qwts[q]*0.5*Mu*dU[i][j]*dU[i][j];
	Energy -= Qwts[q]*divU*P;
      }

    return Energy;
  }

  
  // Compute the residual vector
  void StokesOp::GetVal(const void* state,
			std::vector<std::vector<double>>* funcval) const
  { return GetDVal(state, funcval, nullptr); }
  
  // Compute the residual and stiffness
  void StokesOp::GetDVal(const void* state,
			 std::vector<std::vector<double>>* funcval,
			 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // Zero all output
    SetZero(funcval, dfuncval);
    
    // Get the state
    assert(state!=nullptr && "stk::StokesOp::GetDVal- state not specified");
    const double* config = static_cast<const double*>(state);

    // Access quadrature
    const auto& Qwts = UElm->GetIntegrationWeights(0);
    const int nQuad = static_cast<int>(Qwts.size());

    // Number of velocity dofs
    const int nUdofs = UElm->GetDof(0);

    // Number of pressure dofs
    int nPdofs = PElm->GetDof(0);

    // Spatial dimension
    const int SPD = static_cast<int>(FieldsUsed.size())-1;

    // Velocities and pressure
    double P = 0.;
    std::vector<std::vector<double>> dU(SPD,std::vector<double>(SPD));
    double divU;
    
    // Element residual and stiffness
    for(int q=0; q<nQuad; ++q)
      {
	// Compute velocities, gradient and divergence at this point
	for(int i=0; i<SPD; ++i)
	  for(int j=0; j<SPD; ++j)
	    dU[i][j] = 0.;
	for(int f=0; f<SPD; ++f)
	  for(int a=0; a<nUdofs; ++a)
	    {
	      const double& Ua = config[L2GMap.Map(f, a, ElmNum)];
	      for(int j=0; j<SPD; ++j)
		dU[f][j] += Ua*UElm->GetDShape(f, q, a, j);
	    }

	divU = 0.;
	for(int f=0; f<SPD; ++f)
	  divU += dU[f][f];
	
	// Compute the pressure at this point
	P = 0.;
	for(int a=0; a<nPdofs; ++a)
	  P += config[L2GMap.Map(SPD, a, ElmNum)]*PElm->GetShape(0, q, a);

	// Residue
	if(funcval!=nullptr)
	  {
	    // Variations wrt velocity fields
	    for(int f=0; f<SPD; ++f)
	      for(int a=0; a<nUdofs; ++a)
		{
		  // This variation: V = Na E_f
		  for(int j=0; j<SPD; ++j)
		    (*funcval)[f][a] +=
		      Qwts[q]*Mu*dU[f][j]*UElm->GetDShape(f, q, a, j);
		  
		  (*funcval)[f][a] -= Qwts[q]*P*UElm->GetDShape(f, q, a, f);
		}
	    
	    
	    // Variation wrt the pressure field
	    for(int a=0; a<nPdofs; ++a)
	      (*funcval)[SPD][a] -= Qwts[q]*PElm->GetShape(0, q, a)*divU;
	  }
	
	// Stiffness
	if(dfuncval!=nullptr)
	  {
	    // First variation wrt velocities
	    for(int f=0; f<SPD; ++f)
	      for(int a=0; a<nUdofs; ++a)
		// Second variation wrt velocity
		for(int b=0; b<nUdofs; ++b)
		  {
		    // Nabla(Na E_f) :  Nabla (Nb E_g) -> f=g
		    for(int j=0; j<SPD; ++j)
		      (*dfuncval)[f][a][f][b] +=
			Qwts[q]*Mu*
			UElm->GetDShape(f,q,a,j)*
			UElm->GetDShape(f,q,b,j);
		  }

	    // Second variation wrt pressure: - Nb Div(Na E_f)
	    for(int f=0; f<SPD; ++f)
	      for(int a=0; a<nUdofs; ++a)
		for(int b=0; b<nPdofs; ++b)
		  (*dfuncval)[f][a][SPD][b] -=
		    Qwts[q]*
		    PElm->GetShape(0, q, b)*
		    UElm->GetDShape(f,q,a,f);
	    
	    for(int a=0; a<nPdofs; ++a)
	      for(int g=0; g<SPD; ++g)
		for(int b=0; b<nUdofs; ++b)
		  // Symmetric part
		  (*dfuncval)[SPD][a][g][b] -=
		    Qwts[q]*
		    PElm->GetShape(0, q, a)*
		    UElm->GetDShape(g,q,b,g);
	    
	  }
      }
    
    return;
  }
  
  
  // Consistency test
  bool StokesOp::ConsistencyTest(const void* state,
				 const double pertEPS,
				 const double tolEPS) const
  {
    // Access the state
    assert(state!=nullptr && "stk::StokesOp::ConsistencyTest- Could not access state");
    const double* config = static_cast<const double*>(state);
    const int nTotalDofs = L2GMap.GetTotalNumDof();
    
    // Create element residual and stiffness
    const int nFields = static_cast<int>(FieldsUsed.size());
    std::vector<int> ndofs(nFields);
    for(int f=0; f<nFields; ++f)
      ndofs[f] = GetFieldDof(f);
    
    std::vector<std::vector<double>> resvec(nFields);
    std::vector<std::vector<std::vector<std::vector<double>>>> dresvec(nFields);
    for(int f=0; f<nFields; ++f)
      { resvec[f].resize(ndofs[f]);
	dresvec[f].resize(ndofs[f]);
	for(int a=0; a<ndofs[f]; ++a)
	  { dresvec[f][a].resize(nFields);
	    for(int g=0; g<nFields; ++g)
	      dresvec[f][a][g].resize(ndofs[g]); } }
    std::vector<std::vector<double>> presvec(resvec), mresvec(resvec), nresvec(resvec);
    std::vector<std::vector<std::vector<std::vector<double>>>> ndresvec(dresvec);
    
    // Compute the element residual and stiffness
    GetDVal(state, &resvec, &dresvec);

    // Consistency tests
    std::vector<double> pconfig(nTotalDofs);
    std::vector<double> mconfig(nTotalDofs);
    for(int f=0; f<nFields; ++f)
      for(int a=0; a<ndofs[f]; ++a)
	{
	  // Perturbed configurations
	  std::copy(config, config+nTotalDofs, pconfig.begin());
	  std::copy(config, config+nTotalDofs, mconfig.begin());
	  pconfig[L2GMap.Map(f,a,ElmNum)] += pertEPS;
	  mconfig[L2GMap.Map(f,a,ElmNum)] -= pertEPS;

	  // Energies at perturbed configuration
	  double Eplus = GetEnergy(&pconfig[0]);
	  double Eminus = GetEnergy(&mconfig[0]);
	  
	  // Residuals are perturbed configurations
	  GetVal(&pconfig[0], &presvec);
	  GetVal(&mconfig[0], &mresvec);

	  // Numerical residual
	  nresvec[f][a] = (Eplus-Eminus)/(2.*pertEPS);

	  // Numerical stiffness
	  for(int g=0; g<nFields; ++g)
	    for(int b=0; b<ndofs[g]; ++b)
	      ndresvec[g][b][f][a] = (presvec[g][b]-mresvec[g][b])/(2.*pertEPS);
	}

    // Check consistency
    for(int f=0; f<nFields; ++f)
      for(int a=0; a<ndofs[f]; ++a)
	{
	  assert(std::abs(resvec[f][a]-nresvec[f][a])<tolEPS &&
		 "stk::StokesOp::ConsistencyTest- residuals are inconsistent");

	  for(int g=0; g<nFields; ++g)
	    for(int b=0; b<ndofs[g]; ++b)
	      {
		assert(std::abs(dresvec[f][a][g][b]-ndresvec[f][a][g][b])<tolEPS &&
		       "stk::StokesOp::ConsistencyTest- stiffness is inconsistent");
	      }
	}
    

    // For debugging purposes
    if(0)
      {
	std::cout<<"\n\nResiduals: "<<std::flush;
	for(int f=0; f<nFields; ++f)
	  for(int a=0; a<ndofs[f]; ++a)
	    std::cout<<"\n"<<resvec[f][a]<<" should be "<<nresvec[f][a]<<std::flush;
	
	std::cout<<"\n\nStiffness: "<<std::flush;
	for(int f=0; f<nFields; ++f)
	  for(int a=0; a<ndofs[f]; ++a)
	    for(int g=0; g<nFields; ++g)
	      for(int b=0; b<ndofs[g]; ++b)
		std::cout<<"\n"<<dresvec[f][a][g][b]<<" should be "<<ndresvec[f][a][g][b]<<std::flush;
      }
    return true;
  }

}
