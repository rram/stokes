
#include <stk_P1BubbleShape.h>
#include <cassert>

namespace stk
{
  // 2D specialization
  //---------------------

  // Compute the value of the bubble function
  template<>
  double P1BubbleShape<2>::BubbleVal(const int a, const double* x) const
  { assert(a==3 && "stokes::P1BubbleShape<2>::BubbleVal- Unexpected shape function number");
    return 27.*x[0]*x[1]*(1.-x[0]-x[1]); }


  // Compute the derivatives of the bubble function
  template<>
  double P1BubbleShape<2>::BubbleDVal(const int a, const double* x, const int i) const
  { assert(a==3 && "stokes::P1BubbleShape<2>::BubbleDVal- Unexpected shape function number");
    switch(i)
      {
      case 0: return 27.*x[1]*(1.-2.*x[0]-x[1]);
      case 1: return 27.*x[0]*(1.-x[0]-2.*x[1]);
      }
    assert(false && "stokes::P1BubbleShape<2>::BubbleDVal- Unexpected derivative requested");
  }


  // 3D specialization

  // Compute the value of the bubble function
  template<>
  double P1BubbleShape<3>::BubbleVal(const int a, const double* x) const
  { assert(a==4 && "stokes::P1BubbleShape<3>::BubbleVal- Unexpected shape function number");
    return 256.*x[0]*x[1]*x[2]*(1.-x[0]-x[1]-x[2]); }


  // Compute the derivatives of the bubble function
  template<>
  double P1BubbleShape<3>::BubbleDVal(const int a, const double* x, const int i) const
  { assert(a==4 && "stokes::P1BubbleShape<3>::BubbleDVal- Unexpected shape function number");
    switch(i)
      {
      case 0: return 256.*x[1]*x[2]*(1.-2.*x[0]-x[1]-x[2]);
      case 1: return 256.*x[0]*x[2]*(1.-x[0]-2.*x[1]-x[2]);
      case 2: return 256.*x[0]*x[1]*(1.-x[0]-x[1]-2.*x[2]);
      }
    assert(false && "stokes::P1BubbleShape<3>::BubbleDVal- Unexpected derivative requested");
  }

}
