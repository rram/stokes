// Sriramajayam

#ifndef  STK_STOKES_OPERATION_H
#define  STK_STOKES_OPERATION_H

#include <ElementalOperation.h>
#include <LocalToGlobalMap.h>

namespace stk
{
  //! Assumes that the configuration is of type double*
  //! Assumes that fields are number sequentially as Ux, Uy, (Uz), P
  class StokesOp: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elmnum Element number
    //! \param[in] uelm Velocity element
    //! \param[in] pelm Pressure element
    //! \param[in] mu Viscosity
    //! \param[in] l2gmap Local to global map for this element
    StokesOp(const int elmnum,
	     const Element* uelm, const Element* pelm,
	     const double mu, const LocalToGlobalMap& l2gmap);

    //! Destructor
    virtual ~StokesOp();

    //! Copy constructor
    //! \param[in] obj Object to be copied from
    StokesOp(const StokesOp& obj);

    //! Cloning
    virtual StokesOp* Clone() const override;

    //! Returns the viscosity
    double GetViscosity() const;

    //! Returns the fields used
    virtual const std::vector<int>& GetField() const override;

    //! Returns the number of dofs for a field
    virtual int GetFieldDof(const int field) const override;

    //! Return the velocity element
    const Element* GetVelocityElement() const;

    //! Return the pressure element
    const Element* GetPressureElement() const;
    
    //! Returns the functional
    //! \param[in] config State at which to compute the residual
    virtual double GetEnergy(const void* state) const;
      
    //! Compute the residual vector
    //! \param[in] config State at which to compute the residual
    //! \param[out] funcval Computed elemental force vector
    virtual void GetVal(const void* state,
			std::vector<std::vector<double>>* funcval) const override;

    //! Compute the residual and stiffness
    //! \param[in] config State at which to compute the residual
    //! \param[out] funcval Computed elemental force vector
    //! \param[out] dfuncval Computed elemental stiffness matrix
    virtual void GetDVal(const void* state,
			 std::vector<std::vector<double>>* funcval,
			 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const override;

    //! Consistency test
    //! \param[in] state State at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for dof perturbations
    //! \param[in] tolEPS Tolerance to use for consistency check
    virtual bool ConsistencyTest(const void* state,
				 const double pertEPS,
				 const double tolEPS) const override;

  private:
    const int ElmNum; //!< Element number
    const Element* UElm; //!< Pointer to the velocity element
    const Element* PElm; //!< Pointer to the pressure element
    const double Mu; //!< Viscosity
    const LocalToGlobalMap& L2GMap; //!< Local to global map
    std::vector<int> FieldsUsed; //!< Fields used
  };

}

#endif
