
#ifndef STK_MESH_UTILS_IMPL_H
#define STK_MESH_UTILS_IMPL_H

#include <map>
#include <vector>
#include <list>
#include <cassert>

namespace stk
{
  namespace detail
  {
    // Convert a working mesh into an orphan mesh with nodes numbered from 1
    template<class WMType>
      void Cast2OrphanMesh(const WMType& WM,
			   std::vector<double>& coordinates, std::vector<int>& connectivity,
			   const std::vector<int>& PosVerts, std::vector<int>& posverts,
			   const std::list<std::pair<int, int>>& PosElmFacePairs,
			   std::vector<int>& poselmsnodes)
      {
	const int nodesPerElm = WM.GetNumNodesPerElement();
	const int nodesPerFace = WM.GetNumNodesPerFace();
	const int SPD = WM.GetSpatialDimension();
	
	// Get all the nodes/elements in the working mesh
	std::vector<int> wmnodes;
	WM.GetNodes(wmnodes);
	const auto& ElmList = WM.GetElements();

	// Renumber nodes
	const int nVerts = static_cast<int>(wmnodes.size());
	std::unordered_map<int, int> Old2NewNodeMap({});
	for(int i=0; i<nVerts; ++i)
	  Old2NewNodeMap.insert( std::make_pair(wmnodes[i], i) );

	// Renumber elements and connectivities
	const int nElements = static_cast<int>(ElmList.size());
	connectivity.resize(nodesPerElm*nElements);
	std::unordered_map<int, int> Old2NewElmMap({});
	int ecount = 0;
	for(auto& e:ElmList)
	  {
	    Old2NewElmMap.insert( std::make_pair(e, ecount) ); 
	    const auto* conn = WM.connectivity(e);
	    for(int a=0; a<nodesPerElm; ++a)
	      { auto it = Old2NewNodeMap.find(conn[a]);
		assert(it!=Old2NewNodeMap.end());
		connectivity[nodesPerElm*ecount+a] = it->second+1; } // connectivity is numbered from 1
	    ++ecount;
	  } 
      
	// Coordinates
	coordinates.clear();
	coordinates.resize(nVerts*SPD);
	for(auto& it:Old2NewNodeMap)
	  { const double* X = WM.coordinates( it.first );
	    const int m = it.second;
	    for(int k=0; k<SPD; ++k)
	      coordinates[SPD*m+k] = X[k]; }

	// Renumbered positive vertices
	posverts.clear();
	posverts.reserve(static_cast<int>(PosVerts.size()));
	for(auto& n:PosVerts)
	  { auto it = Old2NewNodeMap.find(n);
	    assert(it!=Old2NewNodeMap.end());
	    posverts.push_back( it->second ); }

	// Renumbered positive elements and their faces
	poselmsnodes.reserve(PosElmFacePairs.size()*(nodesPerFace+1));
	for(auto& ef:PosElmFacePairs)
	  {
	    const int& elm = ef.first;
	    const int& face = ef.second;
	    const int* locnodes = WM.GetLocalFaceNodes(face);
	  
	    // New element number
	    auto it = Old2NewElmMap.find(elm);
	    assert(it!=Old2NewElmMap.end());
	    const int newelm = it->second;
	  
	    poselmsnodes.push_back( newelm );
	    for(int i=0; i<nodesPerFace; ++i)
	      poselmsnodes.push_back( locnodes[i] );
	  }

	return;
      }


  }
  
}  

#endif
