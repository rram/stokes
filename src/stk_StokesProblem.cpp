// Sriramajayam

#include <stk_StokesProblem.h>
#include <cassert>
#include <PlottingUtils.h>
#include <LineQuadratures.h>
#include <MeshUtils.h>
#include <stk_P12DElement6pt.h>
#include <P22DElement.h>
#include <StandardP22DMap.h>
#include <stk_SeqPairedL2GMap.h>

namespace stk
{
  // Constructor
  StokesProblem::StokesProblem(const double mu,
			       msh::StdOrphanMesh& bg,
			       geom::ImplicitManifold& gm,
			       const Meshing_Options& mesh_opts)
    :Mu(mu),
     BG(bg),
     Geom(gm),
     mesh_options(mesh_opts),
     SPD(bg.GetSpatialDimension()),
     is_iteration_setup(false),
     is_iteration_finalized(true),
     is_state_computed(false)
  {
    // Check that PETSc has been initialized
    PetscBool flag;
    PetscErrorCode ierr = PetscInitialized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE && "stk::StokesProblem- PETSc not initialized");
  }

    
  // Destructor
  StokesProblem::~StokesProblem()
  {
    // Check that the iteration has been finalized.
    assert(is_iteration_finalized && "stk::StokesProblem- Iteration not finalized");
  }

  // Helper method to set up a run
  // Set up the mesh, elements, operartions, assembler, sparse data structures
  void StokesProblem::SetupIteration()
  {
    // Iteration cannot already be setup
    assert((is_iteration_setup==false && is_iteration_finalized==true) &&
	   "stk::StokesProblem::SetupIteration- Iteration already setup");
    
    // 2D case
    if(SPD==2)
      Setup2D();

    // 3D case
    // TODO
    
    // Set flags
    is_iteration_setup = true;
    is_state_computed = false;
    is_iteration_finalized = false;
    return;
  }

  // Setup for the 2D case
  void StokesProblem::Setup2D()
  {
    // create a conforming mesh 
    stk::GetConformingMesh<2>(BG, Geom, mesh_options,
			      MD.coordinates, MD.connectivity, PosVerts, PosElmsNodes);
    MD.nodes_element = SPD+1;
    MD.spatial_dimension = SPD;
    MD.nodes = static_cast<int>(MD.coordinates.size()/SPD);
    MD.elements = static_cast<int>(MD.connectivity.size()/MD.nodes_element);

    // Create elements
    Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
    Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
    UElmArray.resize(MD.elements);
    PElmArray.resize(MD.elements);
    for(int e=0; e<MD.elements; ++e)
      {
	const int* conn = &MD.connectivity[3*e];
	UElmArray[e] = new P22DElement<2>(conn[0], conn[1], conn[2]);
	PElmArray[e] = new stk::P12DElement6pt<1>(conn[0], conn[1], conn[2]);
      }

        
    UL2GMap = new StandardP22DMap(UElmArray);
    PL2GMap = new StandardP12DMap(PElmArray);
    L2GMap = new SeqPairedL2GMap(*UL2GMap, *PL2GMap);

    // Create operations
    OpArray.resize(MD.elements);
    for(int e=0; e<MD.elements; ++e)
      OpArray[e] = new StokesOp(e, UElmArray[e], PElmArray[e], Mu, *L2GMap);

    // Assembler
    Asm = new StandardAssembler<StokesOp>(OpArray, *L2GMap);

    // State variables
    const int nTotalDof = L2GMap->GetTotalNumDof();
    state.resize(nTotalDof);
    std::fill(state.begin(), state.end(), 0.);
    
    // Setup sparse data structures
    std::vector<int> nnz;
    Asm->CountNonzeros(nnz);
    PD.Initialize(nnz);
    
    // Index set of velocity dofs
    PetscErrorCode ierr;
    int nVelocityDofs = UL2GMap->GetTotalNumDof();
    std::vector<int> uindices({});
    uindices.reserve(nVelocityDofs);
    for(int i=0; i<nVelocityDofs; ++i)
      uindices.push_back( i );
    ierr = ISCreateGeneral(PETSC_COMM_SELF, nVelocityDofs, &uindices[0],
			   PETSC_COPY_VALUES, &uindxSet);
    CHKERRV(ierr);
    
    // Index set of pressure dofs
    int nPressureDofs = PL2GMap->GetTotalNumDof();
    std::vector<int> pindices({});
    pindices.reserve(nPressureDofs);
    for(int i=0; i<nPressureDofs; ++i)
      pindices.push_back( nVelocityDofs+i );
    ierr = ISCreateGeneral(PETSC_COMM_SELF, nPressureDofs, &pindices[0],
			   PETSC_COPY_VALUES, &pindxSet);
    CHKERRV(ierr);
    
    // Field splitting
    PC pc;
    ierr = KSPGetPC(PD.kspSOLVER, &pc); CHKERRV(ierr);
    ierr = PCFieldSplitSetIS(pc, "0", uindxSet); CHKERRV(ierr);
    ierr = PCFieldSplitSetIS(pc, "1", pindxSet); CHKERRV(ierr);
    
    // -- done --
    return;
  }

  
  // Main functionality: compute velocity and pressure
  void StokesProblem::Compute(const BCParams& bc)
  {
    assert(is_iteration_setup);
    
    // Dirichlet BCs
    const auto& boundary = bc.dirichlet_dofs;
    const auto& bvalues = bc.dirichlet_values;
    const int nbcs = static_cast<int>(boundary.size());
    
    // Resize this state
    const int nTotalDofs = L2GMap->GetTotalNumDof();
    if(static_cast<int>(state.size())<nTotalDofs)
      state.resize(nTotalDofs);
    
    
    // Set the boundary conditions in the state
    std::fill(state.begin(), state.end(), 0.);
    for(int i=0; i<nbcs; ++i)
      state[boundary[i]] = bvalues[i];
    
    std::vector<double> zero(nbcs);
    std::fill(zero.begin(), zero.end(), 0.);

    // Assemble stiffness once & solve multiple times
    PetscErrorCode ierr = KSPSetReusePreconditioner(PD.kspSOLVER, PETSC_TRUE); CHKERRV(ierr);
    Asm->Assemble(&state[0], PD.resVEC, PD.stiffnessMAT);
    
    // Solve
    while(true)
      {
	// Set homogeneous bcs
	PD.SetDirichletBCs(boundary, zero);

	// Solve
	PD.Solve();

	// Update the configuration
	ierr = VecScale(PD.solutionVEC, -1.); CHKERRV(ierr);
	double* sol;
	ierr = VecGetArray(PD.solutionVEC, &sol); CHKERRV(ierr);
	for(int i=0; i<nTotalDofs; ++i)
	  state[i] += sol[i];
	ierr = VecRestoreArray(PD.solutionVEC, &sol); CHKERRV(ierr);
	
	// Check covergence
	if(PD.HasConverged(1.e-10, 1., 1.))
	  break;
	
	// Assemble the residual again
	Asm->Assemble(&state[0], PD.resVEC);
      }

    is_state_computed = true;
    // done
    return;
  }

  
  // Helper method to finalize an iteration
  // Deletes the mesh, elements, operations, assemblers and sparse data structures
  void StokesProblem::FinalizeIteration()
  {
    assert(is_iteration_setup==true && is_iteration_finalized==false);

    // Clean up
    MD.coordinates.clear();
    MD.connectivity.clear();
    PosVerts.clear();
    PosElmsNodes.clear();
    for(auto& e:UElmArray) if(e!=nullptr) delete e;
    for(auto& e:PElmArray) if(e!=nullptr) delete e;
    for(auto& op:OpArray) if(op!=nullptr) delete op;
    if(UL2GMap!=nullptr) delete UL2GMap;
    if(PL2GMap!=nullptr) delete PL2GMap;
    if(L2GMap!=nullptr) delete L2GMap;
    if(Asm!=nullptr) delete Asm;
    PD.Destroy();
    ISDestroy(&uindxSet);
    ISDestroy(&pindxSet);

    is_state_computed = false;
    is_iteration_finalized = true;
    is_iteration_setup = false;

    // done
    return;
  }

  
  // Get the viscosity
  double StokesProblem::GetViscosity() const
  { return Mu; }
    
  // Access the geometry
  const geom::ImplicitManifold& StokesProblem::GetGeometry() const
  { return Geom; }
  
  // Access the background mesh
  msh::StdOrphanMesh& StokesProblem::GetBackgroundMesh()
  { return BG; }
    
  // Access the conforming mesh
  const CoordConn& StokesProblem::GetConformingMesh() const
  { assert(is_iteration_setup);
    return MD; }

  // Access the positive vertices
  const std::vector<int>& StokesProblem::GetPositiveVertices() const
  { assert(is_iteration_setup);
    return PosVerts; }

  // Access positively cut eleents and local nodes on the positive face
  const std::vector<int>& StokesProblem::GetPositiveElmFacePairs() const
  { assert(is_iteration_setup);
    return PosElmsNodes; }
  
  // Access the solution
  const double* StokesProblem::GetState() const
  { assert(is_state_computed);
    return &state[0]; }
  
  // Access the local to global map
  const LocalToGlobalMap& StokesProblem::GetLocalToGlobalMap() const
  { assert(is_iteration_setup);
    return *L2GMap; }

  // Access the element array
  const std::vector<Element*>& StokesProblem::GetVelocityElementArray() const
  { assert(is_iteration_setup);
    return UElmArray; }

  // Access the element array
  const std::vector<Element*>& StokesProblem::GetPressureElementArray() const
  { assert(is_iteration_setup);
    return PElmArray; }
  
  // Access the meshing options
  const Meshing_Options& StokesProblem::GetMeshingOptions() const
  { return mesh_options; }

  // Utility to plot the state
  void StokesProblem::PlotState(const std::string filename) const
  {
    assert(is_state_computed);

    const int nFields = SPD+1;
    std::vector<double> nodalfields(nFields*MD.nodes);
    for(int e=0; e<MD.elements; ++e)
      { const int* conn = &MD.connectivity[MD.nodes_element*e];
	for(int a=0; a<MD.nodes_element; ++a)
	  { const int n = conn[a]-1;
	    for(int f=0; f<nFields; ++f) // Number of fields
	      nodalfields[nFields*n+f] = state[L2GMap->Map(f,a,e)]; } }
    
    PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &nodalfields[0], nFields);
    return;
  }
  

  // Compute forces and moments
  void StokesProblem::ComputeForcesAndMoments(double* F,
					      const double* refX,
					      double* M) const
  { if(SPD==2)
      ComputeForcesAndMoments2D(F, refX, M);
    else
      assert(false); }

  
  // Helper function to compute forces and moments in 2D
  void StokesProblem::ComputeForcesAndMoments2D(double* Frc,
						const double* refX,
						double* Moment) const
  {
    // Initialize to zero
    for(int i=0; i<SPD; ++i)
      Frc[i] = 0.;
    Moment[0] = 0.;
    
    // Shape functions
    Quadratic<2> UShp;
    Linear<2> PShp;

    // Quadrature rules for the three faces
    std::vector<const Quadrature*> FaceQRules(
					      {Triangle_Face_5pt::FaceOne,
						  Triangle_Face_5pt::FaceTwo,
						  Triangle_Face_5pt::FaceThree});
    // Neighbor list
    // Use the fact that +ve faces are along the boundary
    std::vector<std::vector<int>> ElmNbs;
    GetCoordConnFaceNeighborList(MD, ElmNbs);
    
    // Loop over +ve faces of +vely cut triangles
    // Compute the traction
    // Integrate the forces
    double gradU[2][2];
    double sigma[2][2];
    double Identity[2][2] = {{1.,0.},{0.,1.}};
    double P;
    double normal[2];
    double temp;
    double traction[2];
    double rvec[2];
    
    const int nPosElms = static_cast<int>(PosElmsNodes.size()/3);
    for(int i=0; i<nPosElms; ++i)
      {
	const int e = PosElmsNodes[3*i];
	assert(ElmNbs[e][0]<0 || ElmNbs[e][2]<0 || ElmNbs[e][4]<0); // At least 1 boundary face
	
	const int* conn = &MD.connectivity[3*e];
	const int a0 = PosElmsNodes[3*i+1];
	const int a1 = PosElmsNodes[3*i+2];
	const int n0 = conn[a0]-1;
	const int n1 = conn[a1]-1;
	const double* X = &MD.coordinates[2*n0];
	const double* Y = &MD.coordinates[2*n1];
	const double len = std::sqrt((X[0]-Y[0])*(X[0]-Y[0]) + (X[1]-Y[1])*(X[1]-Y[1]));

	// Find the free face
	for(int f=0; f<3; ++f)
	  if(ElmNbs[e][2*f]<0)
	    {
	      // This is a positive face.
	      // Evaluate shape functions and derivatives for U and P fields
	      // at quadrature points along this face
	      const auto& ElmGeom = UElmArray[e]->GetElementGeometry();
	      ShapesEvaluated UShpEval(FaceQRules[f], &UShp, &ElmGeom);
	      ShapesEvaluated PShpEval(FaceQRules[f], &PShp, &ElmGeom);
	      const int nUdof = UShpEval.GetBasisDimension(); assert(nUdof==6);
	      const int nPdof = PShpEval.GetBasisDimension(); assert(nPdof==3);
	      
	      // Gradient of U-shape functions
	      const auto& dUShpVals = UShpEval.GetDShapes();

	      // P-shape functions
	      const auto& PShpVals = PShpEval.GetShapes();
	      
	      // Integration weights
	      const auto& Qwts = UShpEval.GetIntegrationWeights();

	      // Integration points
	      const auto& Qpts = UShpEval.GetQuadraturePointCoordinates();
	      
	      // Number of quadrature points
	      const int nQuad = static_cast<int>(Qwts.size());
	      assert(nQuad==5);
	      assert(static_cast<int>(Qpts.size())==2*nQuad);

	      for(int q=0; q<nQuad; ++q)
		{
		  // Quadrature weight
		  const double Qwt = len*FaceQRules[f]->GetQuadratureWeights(q);

		  // Evaluate gradient of U
		  for(int k=0; k<2; ++k)
		    for(int L=0; L<2; ++L)
		      { gradU[k][L] = 0.;
			for(int a=0; a<nUdof; ++a)
			  gradU[k][L] += state[L2GMap->Map(k,a,e)]*dUShpVals[SPD*nUdof*q+SPD*a+L]; } // P2

		  // Evaluate the pressure
		  P = 0.;
		  for(int a=0; a<nPdof; ++a)
		    P += state[L2GMap->Map(2,a,e)]*PShpVals[nPdof*q+a]; // P1

		  // Stress
		  for(int k=0; k<2; ++k)
		    for(int L=0; L<2; ++L)
		      sigma[k][L] = Mu*0.5*(gradU[k][L]+gradU[L][k]) - P*Identity[k][L];

		  // Normal to the boundary at this point
		  Geom.GetSignedDistance(&Qpts[2*q], temp, normal);
		  temp = std::sqrt(normal[0]*normal[0]+normal[1]*normal[1]);
		  normal[0] /= temp;
		  normal[1] /= temp;
		    
		  
		  // Traction
		  for(int k=0; k<2; ++k)
		    { traction[k] = 0.;
		      for(int L=0; L<2; ++L)
			traction[k] += sigma[k][L]*normal[L]; }
		  
		  // Integrate the traction 
		  for(int k=0; k<2; ++k)
		    Frc[k] += Qwt*traction[k];

		  // Integrate the moment
		  rvec[0] = Qpts[2*q]-refX[0];
		  rvec[1] = Qpts[2*q+1]-refX[1];
		  Moment[0] += Qwt*(rvec[0]*traction[1]-rvec[1]*traction[0]);
		}
	    }
      }
    return;
  }
  
}
    

