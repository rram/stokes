// Sriramajayam

#include <stk_StabilizedStokesProblem.h>
#include <cassert>
#include <PlottingUtils.h>

namespace stk
{
  // Constructor
  StabStokesProblem::StabStokesProblem(const double mu,
				       const double hK,
				       msh::StdOrphanMesh& bg,
				       geom::ImplicitManifold& gm,
				       const Meshing_Options& mesh_opts)
    :Mu(mu),
     hVal(hK),
     BG(bg),
     Geom(gm),
     mesh_options(mesh_opts),
     SPD(bg.GetSpatialDimension()),
     is_iteration_setup(false),
     is_iteration_finalized(true),
     is_state_computed(false)
  {
    // Spatial dimension
    assert(SPD==2 || SPD==3);
    
    // Check that PETSc has been initialized
    PetscBool flag;
    PetscErrorCode ierr = PetscInitialized(&flag); CHKERRV(ierr);
    assert(flag==PETSC_TRUE && "stk::StabStokesProblem- PETSc not initialized");
  }
  
  
  // Destructor
  StabStokesProblem::~StabStokesProblem()
  {
    // Check that the iteration has been finalized.
    assert(is_iteration_finalized && "stk::StabStokesProblem- Iteration not finalized");
  }

  // Helper method to set up a run
  // Set up the mesh, elements, operartions, assembler, sparse data structures
  void StabStokesProblem::SetupIteration()
  {
    // Iteration cannot already be setup
    assert((is_iteration_setup==false && is_iteration_finalized==true) &&
	   "stk::StabStokesProblem::SetupIteration- Iteration already setup");
    
    // 2D/3D cases
    if(SPD==2) Setup2D();
    else Setup3D();
    
    // Set flags
    is_iteration_setup = true;
    is_state_computed = false;
    is_iteration_finalized = false;
    return;
  }

  // Setup an iteration without remeshing
  void StabStokesProblem::SetupIterationWithoutRemeshing(const std::vector<int>& posverts,
							 const std::vector<int>& poselmsnodes)
  {
    // Iteration cannot already be setup
    assert((is_iteration_setup==false && is_iteration_finalized==true) &&
	   "stk::StabStokesProblem::SetupIterationWithoutRemeshing- Iteration already setup");

    PosVerts = posverts;
    PosElmsNodes = poselmsnodes;
    
    // 2D/3D cases
    if(SPD==2) Setup2D(false);
    else Setup3D(false);
    
    // Set flags
    is_iteration_setup = true;
    is_state_computed = false;
    is_iteration_finalized = false;
    return;
  }

  // Main functionality: compute the state
  void StabStokesProblem::Compute(const BCParams& bc)
  {
    assert(is_iteration_setup);

    // Dirichlet BCs
    const auto& boundary = bc.dirichlet_dofs;
    const auto& bvalues = bc.dirichlet_values;
    const int nbcs = static_cast<int>(boundary.size());
    assert(static_cast<int>(bvalues.size())==nbcs);

    // Resize the state if necessary
    const int nTotalDofs = L2GMap->GetTotalNumDof();
    if(static_cast<int>(state.size())<nTotalDofs)
      state.resize(nTotalDofs);

    // Setup boundary conditions in the state
    std::fill(state.begin(), state.end(), 0.);
    for(int i=0; i<nbcs; ++i)
      state[boundary[i]] = bvalues[i];

    // BCs are now homogeneous
    std::vector<double> zero(nbcs);
    std::fill(zero.begin(), zero.end(), 0.);

    // Assemble stiffness once & solve multiple times
    PetscErrorCode ierr = KSPSetReusePreconditioner(PD.kspSOLVER, PETSC_TRUE); CHKERRV(ierr);
    ierr = KSPSetFromOptions(PD.kspSOLVER); CHKERRV(ierr);
    
    // Assemble the stiffness, which is state-independent
    Asm->Assemble(&state[0], PD.resVEC, PD.stiffnessMAT);
    
    // Solve
    while(true)
      {
	// Set bcs
	PD.SetDirichletBCs(boundary, zero);

	// Solve
	PD.Solve();
	
	// Update the configuration
	ierr = VecScale(PD.solutionVEC, -1.); CHKERRV(ierr);
	double* sol;
	ierr = VecGetArray(PD.solutionVEC, &sol); CHKERRV(ierr);
	for(int i=0; i<nTotalDofs; ++i)
	  state[i] += sol[i];
	ierr = VecRestoreArray(PD.solutionVEC, &sol); CHKERRV(ierr);
	
	// Check convergence
	if(PD.HasConverged(1.e-10, 1., 1.))
	  break;
	
	// Assemble the residual at the update configuration
	Asm->Assemble(&state[0], PD.resVEC);
      }

    // Note that the state has been computed
    is_state_computed = true;

    // done
    return;
  }


  // Helper method to finalize the iteration
  // Deletes the mesh, elements, operations, assemblers and sparse data structures
  void StabStokesProblem::FinalizeIteration()
  {
    assert(is_iteration_setup==true && is_iteration_finalized==false);

    // Clean up
    MD.coordinates.clear();
    MD.connectivity.clear();
    PosVerts.clear();
    PosElmsNodes.clear();
    for(auto& e:ElmArray) if(e!=nullptr) delete e;
    for(auto& op:OpArray) if(op!=nullptr) delete op;
    if(L2GMap!=nullptr) delete L2GMap;
    if(Asm!=nullptr) delete Asm;
    PD.Destroy();

    is_state_computed = false;
    is_iteration_finalized = true;
    is_iteration_setup = false;

    // done
    return;
  }


  // Get the viscosity
  double StabStokesProblem::GetViscosity() const
  { return Mu; }
    
  // Access the geometry
  const geom::ImplicitManifold& StabStokesProblem::GetGeometry() const
  { return Geom; }
  
  // Access the background mesh
  msh::StdOrphanMesh& StabStokesProblem::GetBackgroundMesh()
  { return BG; }
    
  // Access the conforming mesh
  const CoordConn& StabStokesProblem::GetConformingMesh() const
  { assert(is_iteration_setup);
    return MD; }

  // Access the positive vertices
  const std::vector<int>& StabStokesProblem::GetPositiveVertices() const
  { assert(is_iteration_setup);
    return PosVerts; }

  // Access positively cut eleents and local nodes on the positive face
  const std::vector<int>& StabStokesProblem::GetPositiveElmFacePairs() const
  { assert(is_iteration_setup);
    return PosElmsNodes; }
  
  // Access the solution
  const double* StabStokesProblem::GetState() const
  { assert(is_state_computed);
    return &state[0]; }

  // Set the solution
  void StabStokesProblem::SetState(const double* sol)
  {
    assert(is_iteration_setup==true && is_iteration_finalized==false);
    const int nTotalDofs = L2GMap->GetTotalNumDof();
    state.assign(sol, sol+nTotalDofs);

    // Switch the state as computed
    is_state_computed = true;
  }
  
  // Access the local to global map
  const LocalToGlobalMap& StabStokesProblem::GetLocalToGlobalMap() const
  { assert(is_iteration_setup);
    return *L2GMap; }

  // Access the element array
  const std::vector<Element*>& StabStokesProblem::GetElementArray() const
  { assert(is_iteration_setup);
    return ElmArray; }

  // Access the meshing options
  const Meshing_Options& StabStokesProblem::GetMeshingOptions() const
  { return mesh_options; }

  // Utility to plot the state
  void StabStokesProblem::PlotState(const std::string filename) const
  {
    assert(is_state_computed);
    PlotTecCoordConnWithNodalFields(filename.c_str(), MD, &state[0], SPD+1);
    return;
  }

  // Compute the net force along positive faces
  void StabStokesProblem::
  ComputeNetForceAndMoment(double* Frc,
			   const double* refX,
			   double* Moment) const
  {
    assert(is_state_computed==true);
    if(SPD==2) ComputeNetForceAndMoment2D(Frc, refX, Moment);
    else ComputeNetForceAndMoment3D(Frc, refX, Moment);
  }
  
}
  
