// Sriramajayam

#ifndef P1_BUBBLE_SHAPE_H
#define P1_BUBBLE_SHAPE_H

#include <Linear.h>

namespace stk
{
  template<int SPD>
    class P1BubbleShape: public Linear<SPD>
    {
    public:
      //! Constructor
      inline P1BubbleShape()
	:Linear<SPD>() {}
  
      //! Destructor
      inline virtual ~P1BubbleShape() {}
  
      //! Copy constructor
      //! \param Obj Object to be copied
      inline P1BubbleShape(const P1BubbleShape& Obj)
	:Linear<SPD>(Obj) {}
  
      //! Cloning
      inline virtual P1BubbleShape* Clone() const override
      { return new P1BubbleShape(*this); }
  
      //! Returns the number of functions
      inline int GetNumberOfFunctions() const  override
      { return Linear<SPD>::GetNumberOfFunctions()+1; }
  
      //! @param a shape function number
      //! @param x first spatial_dimension barycentric coordinates of the point
      //! \warning Does not check range for parameter a
      inline double Val(const int a, const double *x) const override
      {
	if( a<=SPD )
	  return Linear<SPD>::Val(a, x);
	else
	  return BubbleVal(a, x);
      }
  
    //! @param a shape function number
    //! @param x first spatial_dimension barycentric coordinates of the point
    //! @param i partial derivative number 
    //! Returns derivative with respect to the barycentric coordinates
    //! \warning Does not check range for parameters a and i
    inline double DVal(const int a, const double *x, const int i) const override
    {
      if( a<=SPD )
	return Linear<SPD>::DVal(a, x, i);
      else
	return BubbleDVal(a, x, i);
    }

    private:

    //! Compute the value of the bubble function
    //! To be implemented by specializations
    double BubbleVal(const int a, const double* x) const;

    //! Compute the derivatives of the bubble function
    //! To be implemented by specializations
    double BubbleDVal(const int a, const double* x, const int i) const;
    
  };


  // Explicit Instantiations for triangles and tets
  using P12DBubbleShape = P1BubbleShape<2>;
  using P13DBubbleShape = P1BubbleShape<3>;

}

#endif
