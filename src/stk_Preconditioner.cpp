// Sriramajayam

#include <stk_Preconditioner.h>
#include <cassert>
#include <iostream>
#include <cmath>

namespace stk
{
  // Constructor
  Preconditioner::Preconditioner(const int elmnum,
		     const Element* uelm, const Element* pelm,
		     const double mu, const LocalToGlobalMap& l2gmap)
    :DResidue(),
     ElmNum(elmnum),
     UElm(uelm),
     PElm(pelm),
     Mu(mu),
     L2GMap(l2gmap)
  {
    FieldsUsed.clear();
    const int nFields = L2GMap.GetNumFields(0);
    assert(nFields==3);
    assert((nFields==3 || nFields==4) && "stk::Preconditioner- Unexpected number of fields");
    FieldsUsed.resize(nFields);
    for(int f=0; f<nFields; ++f)
      FieldsUsed[f] = f;

    // Check that both elements have the same quadrature rule
    assert( UElm->GetIntegrationWeights(0).size()==PElm->GetIntegrationWeights(0).size() &&
	    "stk::Preconditioner- U and P elements have different quadrature rules");

    // Check assumptions on field numbering
    const int SPD = UElm->GetNumDerivatives(0);
    assert(SPD==2);
    assert(UElm->GetNumFields()==SPD && PElm->GetNumFields()==1);
    const auto& UFields = UElm->GetFields();
    for(int f=0; f<static_cast<int>(UElm->GetFields().size()); ++f)
      assert(UFields[f]==f);
    const auto& PFields = PElm->GetFields();
    assert(PFields[0]==0);
      
  }
  
  // Destructor
  Preconditioner::~Preconditioner() {}

  // Copy constructor
  Preconditioner::Preconditioner(const Preconditioner& obj)
    :DResidue(obj),
     ElmNum(obj.ElmNum),
     UElm(obj.UElm),
     PElm(obj.PElm),
     Mu(obj.Mu),
     L2GMap(obj.L2GMap),
     FieldsUsed(obj.FieldsUsed) {}

  // Cloning
  Preconditioner* Preconditioner::Clone() const
  { return new Preconditioner(*this); }
  
  // Returns the viscosity
  double Preconditioner::GetViscosity() const
  { return Mu; }

  // Returns the fields used
  const std::vector<int>& Preconditioner::GetField() const
  { return FieldsUsed; }

  // Returns the number of dofs for a field
  int Preconditioner::GetFieldDof(const int field) const
  {
    if(field==static_cast<int>(FieldsUsed.size()-1))
      return PElm->GetDof(0);
    else
      return UElm->GetDof(field);
  }

  // Return the velocity element
  const Element* Preconditioner::GetVelocityElement() const
  { return UElm; }
  
  // Return the pressure element
  const Element* Preconditioner::GetPressureElement() const
  { return PElm; }

  
  // Compute the residual vector
  void Preconditioner::GetVal(const void* state,
			std::vector<std::vector<double>>* funcval) const
  { return GetDVal(state, funcval, nullptr); }
  
  // Compute the residual and stiffness
  void Preconditioner::GetDVal(const void* state,
			 std::vector<std::vector<double>>* funcval,
			 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // Zero all output
    SetZero(funcval, dfuncval);
    
    // Access quadrature
    const auto& Qwts = UElm->GetIntegrationWeights(0);
    const int nQuad = static_cast<int>(Qwts.size());

    // Number of velocity dofs
    const int nUdofs = UElm->GetDof(0);

    // Number of pressure dofs
    int nPdofs = PElm->GetDof(0);

    // Spatial dimension
    const int SPD = static_cast<int>(FieldsUsed.size())-1;
    
    // Element stiffness
    for(int q=0; q<nQuad; ++q)
      {
	// Stiffness
	if(dfuncval!=nullptr)
	  {
	    // Velocity variations
	    for(int f=0; f<SPD; ++f)
	      for(int a=0; a<nUdofs; ++a)
		// Second variation wrt velocity
		for(int b=0; b<nUdofs; ++b)
		  {
		    // Nabla(Na E_f) :  Nabla (Nb E_g) -> f=g
		    for(int j=0; j<SPD; ++j)
		      (*dfuncval)[f][a][f][b] +=
			Qwts[q]*Mu*
			UElm->GetDShape(f,q,a,j)*
			UElm->GetDShape(f,q,b,j);
		  }

	    // Pressure variations
	    for(int a=0; a<nPdofs; ++a)
	      for(int b=0; b<nPdofs; ++b)
		(*dfuncval)[SPD][a][SPD][b] -=
		  Qwts[q]*
		  PElm->GetShape(0, q, a)*
		  PElm->GetShape(0, q, b);
	  }
      }
    
    return;
  }
  
  
  // Consistency test
  bool Preconditioner::ConsistencyTest(const void* state,
				 const double pertEPS,
				 const double tolEPS) const
  { return false; }

}
