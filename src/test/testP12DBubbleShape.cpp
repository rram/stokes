
#include <stk_P1BubbleShape.h>
#include <iostream>
#include <cassert>
#include <random>

using namespace stk;

void Test(const P12DBubbleShape& Shp);

int main()
{
  // Create shape functions
  P12DBubbleShape Shp;
  Test(Shp);

  // Test copy
  P12DBubbleShape copy(Shp);
  Test(copy);

  // Test clone
  P12DBubbleShape* clone = Shp.Clone();
  Test(*clone);
  delete clone;
}

void Test(const P12DBubbleShape& Shp)
{
  // Random number generator
  std::random_device rd; 
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(0., 1./3.);

  // Coordinates of evaluation point
  const double lambda[] = {dis(gen), dis(gen)};

  // Test number of shape functions
  const int nShp = Shp.GetNumberOfFunctions();
  assert(nShp==4);
  const int nVars = Shp.GetNumberOfVariables();
  assert(nVars==2);

  // consistency test
  Shp.ConsistencyTest(lambda, 1.e-6);

  // test partition of unity at the nodes
  double x[] = {1.,0., 0.,1., 0.,0.};
  for(int j=0; j<3; ++j)
    { double sum = 0.;
      for(int a=0; a<nShp; ++a)
	sum += Shp.Val(a, x+2*j);
      assert(std::abs(sum-1.)<1.e-8);
      assert(std::abs(Shp.Val(3,x+2*j))<1.e-8); }

  double y[] = {1./3., 1./3.};
  assert(std::abs(Shp.Val(3,y)-1.)<1.e-8);
}
