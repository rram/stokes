
#include <stk_P12DBubbleElement.h>
#include <cassert>
#include <iostream>

using namespace stk;

void Test(const Element& Elm);

int main()
{
  // Coordinates
  std::vector<double> coordinates({2.,0., 0.,3., 0.,0.});
  Triangle<2>::SetGlobalCoordinatesArray(coordinates);

  // Create mini element
  P12DBubbleElement<2> Elm(1,2,3);
  Test(Elm);

  // Copy
  P12DBubbleElement<2> copy(Elm);
  Test(copy);

  // Clone
  auto* clone = copy.Clone();
  Test(*clone);
  delete clone;
}
  

void Test(const Element& Elm)
{
  // Number of fields
  const int nFields = Elm.GetNumFields();
  assert(nFields==2);
  const std::vector<int> fields({0,1});
  
  // Number of dofs
  std::vector<int> ndofs(nFields);
  for(int f=0; f<nFields; ++f)
    ndofs[f] = Elm.GetDof(f);
  for(auto& f:fields)
    assert(ndofs[f]==4);
  
  // Number of derivatives
  const int nVars = 2;
  for(int f=0; f<nFields; ++f)
    assert(Elm.GetNumDerivatives(f)==nVars);

  // Sum integration weights
  const int nQuad = static_cast<int>(Elm.GetIntegrationWeights(0).size());
  double wsum = 0.;
  for(int q=0; q<nQuad; ++q)
    wsum += Elm.GetIntegrationWeights(0)[q];
  assert(std::abs(wsum-3.)<1.e-8);

  // Partition of unity for the first 3 dofs
  for(int q=0; q<nQuad; ++q)
    { 
      for(int f=0; f<nFields; ++f)
	{
	  double sum = 0.;
	  double dsum = 0.;
	  for(int a=0; a<ndofs[f]-1; ++a) // Don't include the bubble function
	    {
	      sum += Elm.GetShape(f, q, a);
	      for(int i=0; i<nVars; ++i)
		dsum += Elm.GetDShape(f, q, a, i);
	    }
	  assert(std::abs(sum-1.)<1.e-8);
	  assert(std::abs(dsum)<1.e-8);
	}
    }
}
