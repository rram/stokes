
#include <stk_P1BubbleL2GMap.h>
#include <stk_P12DBubbleElement.h>
#include <cassert>
#include <iostream>
#include <algorithm>

using namespace stk;

void Test(const int* conn, const LocalToGlobalMap& L2GMap);

int main()
{
  std::vector<double> coordinates({0.5,-1., 1.,0., 0.,0., 0.5,1.});
  Triangle<2>::SetGlobalCoordinatesArray(coordinates);
  std::vector<Element*> ElmArray(2);
  const int conn[] = {1,2,3, 2,4,3};
  ElmArray[0] = new P12DBubbleElement<2>(conn[0], conn[1], conn[2]);
  ElmArray[1] = new P12DBubbleElement<2>(conn[3], conn[4], conn[5]);
  
  // Create local to global map
  P1BubbleL2GMap L2GMap(ElmArray);
  Test(conn, L2GMap);
  
  delete ElmArray[0];
  delete ElmArray[1];
}


void Test(const int* conn, const LocalToGlobalMap& L2GMap)
{
  const int nElements = 2;
  const int nFields = 2;
  const std::vector<int> ndofs({4,4});
  const int nTotalDofs = 12;
  const int nNodes = 4;
  const int SPD = 2;
  
  assert(L2GMap.GetNumElements()==nElements);
  for(int e=0; e<nElements; ++e)
    {
      assert(L2GMap.GetNumFields(e)==nFields);
      for(int f=0; f<nFields; ++f)
	assert(L2GMap.GetNumDofs(e,f)==ndofs[f]);
    }

  assert(L2GMap.GetTotalNumDof()==nTotalDofs);

  // Check nodal dof maps
  for(int e=0; e<nElements; ++e)
    for(int f=0; f<nFields; ++f)
      {
	for(int a=0; a<ndofs[f]-1; ++a)
	  { int n = conn[(SPD+1)*e+a]-1;
	    assert(L2GMap.Map(f,a,e)==nFields*n+f);
	  }
	assert(L2GMap.Map(f, SPD+1, e)==nNodes*nFields+SPD*e+f);
      }
}
