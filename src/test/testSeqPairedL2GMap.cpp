// Sriramajayam

#include <stk_SeqPairedL2GMap.h>
#include <stk_P1BubbleL2GMap.h>
#include <P12DElement.h>
#include <stk_P12DBubbleElement.h>
#include <cassert>

using namespace stk;

void Test(const int* conn, const LocalToGlobalMap& L2GMap);

int main()
{
  std::vector<double> coordinates({0.5,-1., 1.,0., 0.,0., 0.5,1.});
  Triangle<2>::SetGlobalCoordinatesArray(coordinates);
  const int conn[] = {1,2,3, 2,4,3};

  // Velocity elements
  std::vector<Element*> ElmArray_1(2);
  ElmArray_1[0] = new P12DBubbleElement<2>(conn[0], conn[1], conn[2]);
  ElmArray_1[1] = new P12DBubbleElement<2>(conn[3], conn[4], conn[5]);
  P1BubbleL2GMap L2GMap_1(ElmArray_1);

  // Pressure elements
  std::vector<Element*> ElmArray_2(2);
  ElmArray_2[0] = new P12DElement<1>(conn[0], conn[1], conn[2]);
  ElmArray_2[1] = new P12DElement<1>(conn[3], conn[4], conn[5]);
  StandardP12DMap L2GMap_2(ElmArray_2);

  // Paired mapping
  SeqPairedL2GMap L2GMap(L2GMap_1, L2GMap_2);
  
  Test(conn, L2GMap);
  
  delete ElmArray_1[0];
  delete ElmArray_2[1];
}



void Test(const int* conn, const LocalToGlobalMap& L2GMap)
{
  const int nElements = 2;
  const int nFields = 3;
  const std::vector<int> ndofs({4,4,3});
  const int nTotalDofs = 16;
  const int nNodes = 4;
  const int SPD = 2;
  const int nVelocityDofs = 12;
  
  assert(L2GMap.GetNumElements()==nElements);
  for(int e=0; e<nElements; ++e)
    {
      assert(L2GMap.GetNumFields(e)==nFields);
      for(int f=0; f<nFields; ++f)
	assert(L2GMap.GetNumDofs(e,f)==ndofs[f]);
    }

  assert(L2GMap.GetTotalNumDof()==nTotalDofs);

  // Check dof mapping
  for(int e=0; e<nElements; ++e)
    {
      // Velocity dofs
      for(int f=0; f<nFields-1; ++f)
	{
	  for(int a=0; a<ndofs[f]-1; ++a)
	    { int n = conn[(SPD+1)*e+a]-1;
	      assert(L2GMap.Map(f,a,e)==SPD*n+f);
	    }
	  assert(L2GMap.Map(f, SPD+1, e)==nNodes*SPD+SPD*e+f);
	}
      
      // Pressure dofs
      {
	int f = nFields-1;
	for(int a=0; a<ndofs[f]; ++a)
	  { int n = conn[(SPD+1)*e+a]-1;
	    assert(L2GMap.Map(f,a,e)==nVelocityDofs + n); }
      }
    }
  
  return;
}
