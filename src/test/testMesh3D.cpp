
#include <stk_MeshUtils.h>
#include <MeshUtils.h>
#include <PlottingUtils.h>
#include <geom_LevelSetManifold.h>
#include <omp.h>

// Level set function for a sphere
void ImplicitSphere(const double* X, double& F, double* dF, double* d2F, void* params);

int main()
{
  const int nThreads = 1;
  omp_set_num_threads(nThreads);

  // Create an implicit manifold for a sphere
  geom::LevelSetFunction SphFunc = ImplicitSphere;
  geom::NLSolverParams nlparams({.ftol=1.e-6, .ttol=1.e-6, .max_iter=25});
  geom::LevelSetManifold<3> Geom(nlparams, SphFunc);

  // Read the background mesh
  const int SPD = 3;
  std::vector<int> bgconn;
  std::vector<double> bgcoord;
  msh::ReadTecplotFile("AcuteCubeTet.msh", SPD, bgcoord, bgconn);
  msh::SeqCoordinates BGCoord(SPD, bgcoord);
  msh::StdTetConnectivity BGConn(bgconn);
  msh::StdTetMesh BG(BGCoord, BGConn);
  msh::PlotTecStdTetMesh("BG.tec", BG);

  // Meshing options
  stk::Meshing_Options mesh_options({
      .nMaxThreads=1, 
	.MaxVertValency=40,
	.MaxElmValency=40,
	.nSteps=4,
	.nIters=10,
	.nBdSamples=20});
  
  // Create a conforming mesh
  std::vector<int> conf_conn;
  std::vector<double> conf_coord;
  std::vector<int> PosVerts, PosElmsNodes;
  stk::GetConformingMesh<SPD>(BG, Geom, mesh_options, conf_coord, conf_conn, PosVerts, PosElmsNodes);
  for(auto& i:conf_conn) --i;
  msh::SeqCoordinates MDcoord(SPD, conf_coord);
  msh::StdTetConnectivity MDconn(conf_conn);
  msh::StdTetMesh MD(MDcoord, MDconn);
  msh::PlotTecStdTetMesh("MD.tec", MD);

  // Consistency check for PosVerts and PosElmsNodes
  std::set<int> PosVertsSet1(PosVerts.begin(), PosVerts.end());
  std::set<int> PosVertsSet2({});
  const int nPosElms = static_cast<int>(PosElmsNodes.size()/4);
  for(int i=0; i<nPosElms; ++i)
    {
      const int e = PosElmsNodes[4*i];
      const int* conn = MD.connectivity(e);
      const int a0 = PosElmsNodes[4*i+1];
      const int a1 = PosElmsNodes[4*i+2];
      const int a2 = PosElmsNodes[4*i+3];
      PosVertsSet2.insert( conn[a0] );
      PosVertsSet2.insert( conn[a1] );
      PosVertsSet2.insert( conn[a2] );
    }
  assert(PosVertsSet1==PosVertsSet2);
}



void ImplicitSphere(const double* X, double& F, double* dF, double* d2F, void* params)
{
  const double rad2 = 0.5*0.5;
  F = rad2-X[0]*X[0]-X[1]*X[1]-X[2]*X[2];
  if(dF!=nullptr)
    { dF[0] = -2.*X[0]; dF[1] = -2.*X[1]; dF[2] = -2.*X[2]; }
  if(d2F!=nullptr)
    { d2F[0] = -2.; d2F[1] = 0.;  d2F[2] = 0.;
      d2F[3] = 0.;  d2F[4] = -2.; d2F[5] = 0.;
      d2F[6] = 0.;  d2F[7] = 0.;  d2F[8] = -2.; }
  return;
}
