// Sriramajayam

#include <stk_StokesOperation.h>
#include <stk_P12DBubbleElement.h>
#include <stk_P12DElement6pt.h>
#include <stk_P1BubbleL2GMap.h>
#include <stk_SeqPairedL2GMap.h>
#include <random>
#include <iostream>

using namespace stk;

void Test(const StokesOp& Op, const LocalToGlobalMap& L2GMap, const double Mu);

int main()
{


  // 2D test
  std::vector<double> coord2D({0.,0., 1.,0.75, 0.5, 2.});
  Triangle<2>::SetGlobalCoordinatesArray(coord2D);

  std::vector<Element*> UElmArray(1);
  UElmArray[0] = new P12DBubbleElement<2>({1,2,3});
  P1BubbleL2GMap UL2GMap(UElmArray);

  std::vector<Element*> PElmArray(1);
  PElmArray[0] = new P12DElement6pt<1>({1,2,3});
  StandardP12DMap PL2GMap(PElmArray);

  SeqPairedL2GMap L2GMap(UL2GMap, PL2GMap);
  
  StokesOp Op(0, UElmArray[0], PElmArray[0], 1./std::sqrt(3.), L2GMap);
  Test(Op, L2GMap, 1./std::sqrt(3.));
 

  delete UElmArray[0];
  delete PElmArray[0];
}


void Test(const StokesOp& Op, const LocalToGlobalMap& L2GMap,  const double Mu)
{
  const double TOL = 1.e-6;
  const int SPD = 2;
  assert(std::abs(Op.GetViscosity()-Mu)<TOL);
  assert(static_cast<int>(Op.GetField().size())==SPD+1);
  for(int f=0; f<SPD+1; ++f)
    assert(Op.GetField()[f]==f);
  for(int f=0; f<SPD; ++f)
    assert(Op.GetFieldDof(f)==SPD+2);
  assert(Op.GetFieldDof(SPD)==SPD+1);

  // Random number generator: dof values
  std::random_device rd; 
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-1.,1.);
  const int nTotalDof = L2GMap.GetTotalNumDof();
  std::vector<double> state(nTotalDof);
  for(int a=0; a<nTotalDof; ++a)
    state[a] = dis(gen);

  assert(Op.ConsistencyTest(&state[0], TOL, TOL));
  return;
}
