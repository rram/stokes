// Sriramajayam

#include <stk_StabilizedStokesOperation.h>
#include <stk_P12DElement6pt.h>
#include <random>
#include <iostream>

using namespace stk;

void Test(const StabStokesOp& Op,
	  const LocalToGlobalMap& L2GMap,
	  const double Mu, const double hK);

int main()
{
  // 2D test
  std::vector<double> coord2D({0.,0., 1.,0.75, 0.5, 2.});
  Triangle<2>::SetGlobalCoordinatesArray(coord2D);

  std::vector<Element*> ElmArray(1);
  ElmArray[0] = new P12DElement6pt<3>({1,2,3});
  StandardP12DMap L2GMap(ElmArray);
  
  // Random number generator: mu and hK
  std::random_device rd; 
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(0.25, std::sqrt(10.));
  const double Mu = dis(gen);
  const double hK = dis(gen);
  StabStokesOp Op(0, ElmArray[0], Mu, hK, L2GMap);
  Test(Op, L2GMap, Mu, hK);

  StabStokesOp copy(Op);
  Test(copy, L2GMap, Mu, hK);

  StabStokesOp* clone = copy.Clone();
  Test(*clone, L2GMap, Mu, hK);
  
  delete clone;
  delete ElmArray[0];
}


void Test(const StabStokesOp& Op, const LocalToGlobalMap& L2GMap,  const double Mu, const double hK)
{
  const double TOL = 1.e-6;
  const int SPD = 2;
  assert(std::abs(Op.GetViscosity()-Mu)<TOL);
  assert(std::abs(Op.GetStabilizationParameter()-hK*hK/(12.*Mu))<TOL);
  assert(static_cast<int>(Op.GetField().size())==SPD+1);
  for(int f=0; f<SPD+1; ++f)
    assert(Op.GetField()[f]==f);
  for(int f=0; f<SPD+1; ++f)
    assert(Op.GetFieldDof(f)==SPD+1);
  
  // Random number generator: dof values
  std::random_device rd; 
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-1.,1.);
  const int nTotalDof = L2GMap.GetTotalNumDof();
  std::vector<double> state(nTotalDof);
  for(int a=0; a<nTotalDof; ++a)
    state[a] = dis(gen);

  assert(Op.ConsistencyTest(&state[0], TOL, TOL));
  return;
}
