// Sriramajayam

#include <stk_MeshUtils.h>
#include <MeshUtils.h>
#include <PlottingUtils.h>
#include <geom_LevelSetManifold.h>
#include <omp.h>

// Level set function for a circle
void ImplicitCircle(const double* X, double& F, double* dF, double* d2F, void* params);

// Create a background mesh of equilateral triangles
void GetEquilateralMesh(std::vector<double>& coordinates, std::vector<int>& connectivity);

int main()
{
  const int nThreads = 1;
  omp_set_num_threads(nThreads);

  // Create an implicit manifold for a cirlce
  geom::LevelSetFunction CircleFunc = ImplicitCircle;
  geom::NLSolverParams nlparams({.ftol=1.e-6, .ttol=1.e-6, .max_iter=25});
  geom::LevelSetManifold<2> Geom(nlparams, CircleFunc);
  
  // Create a background mesh
  std::vector<int> bgconn;
  std::vector<double> bgcoord;
  GetEquilateralMesh(bgcoord, bgconn);
  msh::SeqCoordinates BGCoord(2, bgcoord);
  msh::StdTriConnectivity BGConn(bgconn);
  msh::StdTriMesh BG(BGCoord, BGConn);
  msh::PlotTecStdTriMesh("BG.tec", BG);

  // Meshing options
  stk::Meshing_Options mesh_options({
      .nMaxThreads=1, 
	.MaxVertValency=6,
	.MaxElmValency=6,
	.nSteps=4,
	.nIters=10,
	.nBdSamples=0});
  
  // Create a conforming mesh
  std::vector<int> conf_conn;
  std::vector<double> conf_coord;
  std::vector<int> PosVerts, PosElmsNodes;
  stk::GetConformingMesh<2>(BG, Geom, mesh_options, conf_coord, conf_conn, PosVerts, PosElmsNodes);
  for(auto& i:conf_conn) --i;
  msh::SeqCoordinates MDcoord(2, conf_coord);
  msh::StdTriConnectivity MDconn(conf_conn);
  msh::StdTriMesh MD(MDcoord, MDconn);
  msh::PlotTecStdTriMesh("MD.tec", MD);

  // Consistency check for PosVerts and PosElmsNodes
  std::set<int> PosVertsSet1(PosVerts.begin(), PosVerts.end());
  std::set<int> PosVertsSet2({});
  const int nPosElms = static_cast<int>(PosElmsNodes.size()/3);
  for(int i=0; i<nPosElms; ++i)
    {
      const int e = PosElmsNodes[3*i];
      const int* conn = MD.connectivity(e);
      const int a0 = PosElmsNodes[3*i+1];
      const int a1 = PosElmsNodes[3*i+2];
      PosVertsSet2.insert( conn[a0] );
      PosVertsSet2.insert( conn[a1] );
    }
  assert(PosVertsSet1==PosVertsSet2);
}


// Create a background mesh of equilateral triangles
void GetEquilateralMesh(std::vector<double>& coord,
			std::vector<int>& conn)
{
  // Connectivity
  int connarray[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1};
  conn.assign(connarray, connarray+6*3);
  coord.clear();
  coord.reserve(2*7);
  for(int k=0; k<2; ++k)
    coord.push_back( 0. );
  for(int i=0; i<6; ++i)
    {
      double theta = double(i)*(M_PI/3.);
      coord.push_back( 1.*std::cos(theta) );
      coord.push_back( 1.*std::sin(theta) );
    }

  // Subdivide
  for(int i=0; i<4; i++)
    msh::SubdivideTriangles(2, conn, coord);

  return;
}


void ImplicitCircle(const double* X, double& F, double* dF, double* d2F, void* params)
{
  const double rad2 = 0.6*0.6;
  F = rad2-X[0]*X[0]-X[1]*X[1];
  if(dF!=nullptr)
    { dF[0] = -2.*X[0]; dF[1] = -2.*X[1]; }
  if(d2F!=nullptr)
    { d2F[0] = -2.; d2F[1] = 0.;
      d2F[2] = 0.; d2F[3] = -2.; }
  return;
}
