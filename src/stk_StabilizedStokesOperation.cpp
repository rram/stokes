// Sriramajayam

#include <stk_StabilizedStokesOperation.h>
#include <cassert>
#include <iostream>
#include <cmath>

namespace stk
{
  // Constructor
  StabStokesOp::StabStokesOp(const int elmnum, const Element* elm,
			     const double mu, const double hK,
			     const LocalToGlobalMap& l2gmap)
    :DResidue(),
     ElmNum(elmnum),
     Elm(elm),
     Mu(mu),
     L2GMap(l2gmap),
     Tau((hK*hK)/(12.*mu)) // Gustavo Buscaglia's estimate
  {
    FieldsUsed.clear();
    const int nFields = L2GMap.GetNumFields(0);
    assert((nFields==3 || nFields==4) && "stk::StabStokesOp- Unexpected number of fields"); // 2D or 3D
    FieldsUsed.resize(nFields);
    for(int f=0; f<nFields; ++f)
      FieldsUsed[f] = f;
  }

  
  // Destructor
  StabStokesOp::~StabStokesOp() {}

  
  // Copy constructor
  StabStokesOp::StabStokesOp(const StabStokesOp& obj)
    :DResidue(obj),
     ElmNum(obj.ElmNum),
     Elm(obj.Elm),
     Mu(obj.Mu),
     L2GMap(obj.L2GMap),
     FieldsUsed(obj.FieldsUsed),
     Tau(obj.Tau) {}

  // Cloning
  StabStokesOp* StabStokesOp::Clone() const
  { return new StabStokesOp(*this); }
  
  // Returns the viscosity
  double StabStokesOp::GetViscosity() const
  { return Mu; }
  
  // Returns the fields used
  const std::vector<int>& StabStokesOp::GetField() const
  { return FieldsUsed; }
  
  // Return the velocity element
  const Element* StabStokesOp::GetElement() const
  { return Elm; }

  // Returns the number of dofs for a field
  int StabStokesOp::GetFieldDof(const int field) const
  { return Elm->GetDof(field); }
  
  // Returns the stabilization parameter
  double StabStokesOp::GetStabilizationParameter() const
  { return Tau; }
  
  // Returns the functional
  double StabStokesOp::GetEnergy(const void* state) const
  {
    // Get the state
    assert(state!=nullptr && "stk::StabStokesOp::GetEnergy()- state is not specified");
    const double* config = static_cast<const double*>(state);

    // Access the quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(0);
    const int nQuad = static_cast<int>(Qwts.size());

    // Spatial dimension =  number of velocity fields
    const int SPD = static_cast<int>(FieldsUsed.size())-1;
    
    // Pressure field number
    const int PField = SPD;

    // Assume identical number of dofs for all fields
    const int ndofs = Elm->GetDof(0);
    
    // Velocity and pressure
    double P;
    std::vector<double> dP(SPD);
    double divU;
    std::vector<std::vector<double>> dU(SPD, std::vector<double>(SPD));
    
    // Integrate the functional
    double Energy = 0.;
    for(int q=0; q<nQuad; ++q)
      {
	// Velocity and its gradient at this point
	for(int i=0; i<SPD; ++i)
	  for(int j=0; j<SPD; ++j)
	    dU[i][j] = 0.;
	for(int f=0; f<SPD; ++f)
	  for(int a=0; a<ndofs; ++a)
	    {
	      const double& Ua = config[L2GMap.Map(f, a, ElmNum)];
	      for(int j=0; j<SPD; ++j)
		dU[f][j] += Ua*Elm->GetDShape(f, q, a, j);
	    }
	
	// Divergence of U
	divU = 0.;
	for(int i=0; i<SPD; ++i)
	  divU += dU[i][i];

	// Pressure
	P = 0.;
	std::fill(dP.begin(), dP.end(), 0.);
	for(int a=0; a<ndofs; ++a)
	  {
	    const auto& Pa = config[L2GMap.Map(PField, a, ElmNum)];
	    P += Pa*Elm->GetShape(PField, q, a);
	    for(int j=0; j<SPD; ++j)
	      dP[j] += Pa*Elm->GetDShape(PField, q, a, j);
	  }

	// Update the energy
	// 1.
	for(int i=0; i<SPD; ++i)
	  for(int j=0; j<SPD; ++j)
	    Energy += Qwts[q]*0.5*Mu*dU[i][j]*dU[i][j];
	// 2.
	Energy -= Qwts[q]*divU*P;
	// 3.
	for(int j=0; j<SPD; ++j)
	  Energy -= 0.5*Qwts[q]*Tau*dP[j]*dP[j];
      }
    
    return Energy;
  }
  
  
  // Compute the residual vector
  void StabStokesOp::GetVal(const void* state,
			    std::vector<std::vector<double>>* funcval) const
  { return GetDVal(state, funcval, nullptr); }

  // Compute the residual and stiffness
  void StabStokesOp::GetDVal(const void* state,
			     std::vector<std::vector<double>>* funcval,
			     std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // Zero all output
    SetZero(funcval, dfuncval);
    
    // Get the state
    assert(state!=nullptr && "stk::StabStokesOp::GetDVal- state not specified");
    const double* config = static_cast<const double*>(state);

    // Access quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(0);
    const int nQuad = static_cast<int>(Qwts.size());

    // Assume identical dofs for all fields
    const int ndofs = Elm->GetDof(0);

    // Spatial dimension = number of velocity fields
    const int SPD = static_cast<int>(FieldsUsed.size())-1;

    // Pressure field
    const int PField = SPD;
    
    // Velocities and pressure
    double P = 0.;
    std::vector<double> dP(SPD);
    std::vector<std::vector<double>> dU(SPD,std::vector<double>(SPD));
    double divU;
    
    // Element residual and stiffness
    for(int q=0; q<nQuad; ++q)
      {
	// Compute velocities, gradient and divergence at this point
	for(int i=0; i<SPD; ++i)
	  for(int j=0; j<SPD; ++j)
	    dU[i][j] = 0.;
	for(int f=0; f<SPD; ++f)
	  for(int a=0; a<ndofs; ++a)
	    {
	      const double& Ua = config[L2GMap.Map(f, a, ElmNum)];
	      for(int j=0; j<SPD; ++j)
		dU[f][j] += Ua*Elm->GetDShape(f, q, a, j);
	    }

	divU = 0.;
	for(int f=0; f<SPD; ++f)
	  divU += dU[f][f];
	
	// Compute the pressure and its derivative at this point
	P = 0.;
	std::fill(dP.begin(), dP.end(), 0.);
	for(int a=0; a<ndofs; ++a)
	  {
	    const double& Pa = config[L2GMap.Map(PField, a, ElmNum)];
	    P += Pa*Elm->GetShape(PField, q, a);
	    for(int j=0; j<SPD; ++j)
	      dP[j] += Pa*Elm->GetDShape(PField, q, a, j);
	  }

	// Residue
	if(funcval!=nullptr)
	  {
	    // Variations wrt velocity fields
	    for(int f=0; f<SPD; ++f)
	      for(int a=0; a<ndofs; ++a)
		{
		  // This variation: V = Na E_f
		  for(int j=0; j<SPD; ++j)
		    (*funcval)[f][a] +=
		      Qwts[q]*Mu*dU[f][j]*Elm->GetDShape(f, q, a, j); // Mu dU:dV

		  (*funcval)[f][a] -= Qwts[q]*P*Elm->GetDShape(f, q, a, f); // P div(V)
		}
	    
	    
	    // Variation wrt the pressure field
	    for(int a=0; a<ndofs; ++a)
	      {
		// This variation q = Na
		(*funcval)[PField][a] -= Qwts[q]*Elm->GetShape(0, q, a)*divU; // Q div(U)
		for(int j=0; j<SPD; ++j)
		  (*funcval)[SPD][a] -= Qwts[q]*Tau*dP[j]*Elm->GetDShape(PField, q, a, j); // tau dP.dQ
	      }
	  }
	
	// Stiffness
	if(dfuncval!=nullptr)
	  {
	    // First & second variation wrt velocities
	    for(int f=0; f<SPD; ++f)
	      for(int a=0; a<ndofs; ++a)
		for(int b=0; b<ndofs; ++b)
		  for(int j=0; j<SPD; ++j)
		    (*dfuncval)[f][a][f][b] +=        // Mu dV:dW
		      Qwts[q]*Mu*
		      Elm->GetDShape(f,q,a,j)*
		      Elm->GetDShape(f,q,b,j);
	    
	    // First variation wrt velocity
	    // Second variation wrt pressure
	    // +
	    // Symmetric part
	    for(int f=0; f<SPD; ++f)
	      for(int a=0; a<ndofs; ++a)
		for(int b=0; b<ndofs; ++b)
		  {
		    (*dfuncval)[f][a][PField][b] -=    // Q div(V)
		      Qwts[q]*
		      Elm->GetDShape(f,q,a,f)*
		      Elm->GetShape(PField, q, b);
		    
		    (*dfuncval)[PField][b][f][a] -=  // Q div(V)
		      Qwts[q]*
		      Elm->GetDShape(f,q,a,f)*
		      Elm->GetShape(PField, q, b);
		  }
		    
	    // Both variations wrt pressure
	    for(int a=0; a<ndofs; ++a)
	      for(int b=0; b<ndofs; ++b)
		for(int j=0; j<SPD; ++j)
		  (*dfuncval)[SPD][a][SPD][b] -=   // tau dQ.dZ
		    Qwts[q]*Tau*
		    Elm->GetDShape(PField, q, a, j)*
		    Elm->GetDShape(PField, q, b, j);
	  }
      }
    
    return;
  }

  // Consistency test
  bool StabStokesOp::ConsistencyTest(const void* state,
				     const double pertEPS,
				     const double tolEPS) const
  {
    // Access the state
    assert(state!=nullptr && "stk::StabStokesOp::ConsistencyTest- Could not access state");
    const double* config = static_cast<const double*>(state);
    const int nTotalDofs = L2GMap.GetTotalNumDof();

    // Number of velocity fields
    const int nFields = static_cast<int>(FieldsUsed.size());
    
    // Assume identical number of dofs for all fields
    const int ndofs = GetFieldDof(0);
    
    // Create element residual and stiffness
    std::vector<std::vector<double>> resvec(nFields);
    std::vector<std::vector<std::vector<std::vector<double>>>> dresvec(nFields);
    for(int f=0; f<nFields; ++f)
      { resvec[f].resize(ndofs);
	dresvec[f].resize(ndofs);
	for(int a=0; a<ndofs; ++a)
	  { dresvec[f][a].resize(nFields);
	    for(int g=0; g<nFields; ++g)
	      dresvec[f][a][g].resize(ndofs); } }
    std::vector<std::vector<double>> presvec(resvec), mresvec(resvec), nresvec(resvec);
    std::vector<std::vector<std::vector<std::vector<double>>>> ndresvec(dresvec);
    
    // Compute the element residual and stiffness
    GetDVal(state, &resvec, &dresvec);

    // Consistency tests
    std::vector<double> pconfig(nTotalDofs);
    std::vector<double> mconfig(nTotalDofs);
    for(int f=0; f<nFields; ++f)
      for(int a=0; a<ndofs; ++a)
	{
	  // Perturbed configurations
	  std::copy(config, config+nTotalDofs, pconfig.begin());
	  std::copy(config, config+nTotalDofs, mconfig.begin());
	  pconfig[L2GMap.Map(f,a,ElmNum)] += pertEPS;
	  mconfig[L2GMap.Map(f,a,ElmNum)] -= pertEPS;

	  // Energies at perturbed configuration
	  double Eplus = GetEnergy(&pconfig[0]);
	  double Eminus = GetEnergy(&mconfig[0]);
	  
	  // Residuals are perturbed configurations
	  GetVal(&pconfig[0], &presvec);
	  GetVal(&mconfig[0], &mresvec);

	  // Numerical residual
	  nresvec[f][a] = (Eplus-Eminus)/(2.*pertEPS);

	  // Numerical stiffness
	  for(int g=0; g<nFields; ++g)
	    for(int b=0; b<ndofs; ++b)
	      ndresvec[g][b][f][a] = (presvec[g][b]-mresvec[g][b])/(2.*pertEPS);
	}

    
    // Check consistency
    for(int f=0; f<nFields; ++f)
      for(int a=0; a<ndofs; ++a)
	{
	  assert(std::abs(resvec[f][a]-nresvec[f][a])<tolEPS &&
		 "stk::StabStokesOp::ConsistencyTest- residuals are inconsistent");

	  for(int g=0; g<nFields; ++g)
	    for(int b=0; b<ndofs; ++b)
	      {
		assert(std::abs(dresvec[f][a][g][b]-ndresvec[f][a][g][b])<tolEPS &&
		       "stk::StabStokesOp::ConsistencyTest- stiffness is inconsistent");
	      }
	}
    

    // For debugging purposes
    if(0)
      {
	std::cout<<"\n\nResiduals: "<<std::flush;
	for(int f=0; f<nFields; ++f)
	  for(int a=0; a<ndofs; ++a)
	    std::cout<<"\n"<<resvec[f][a]<<" should be "<<nresvec[f][a]<<std::flush;
	
	std::cout<<"\n\nStiffness: "<<std::flush;
	for(int f=0; f<nFields; ++f)
	  for(int a=0; a<ndofs; ++a)
	    for(int g=0; g<nFields; ++g)
	      for(int b=0; b<ndofs; ++b)
		std::cout<<"\n"<<dresvec[f][a][g][b]<<" should be "<<ndresvec[f][a][g][b]<<std::flush;
      }
    return true;
  }

}
