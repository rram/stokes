// Sriramajayam

#ifndef STK_STABILZED_STOKES_PROBLEM_H
#define STK_STABILZED_STOKES_PROBLEM_H

#include <stk_MeshUtils.h>
#include <stk_StabilizedStokesOperation.h>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <Assembler.h>
#include <geom_ImplicitManifold.h>
#include <External.h>
#include <PetscData.h>
#include <stk_BCParams.h>

namespace stk
{
  class StabStokesProblem
  {
  public:
    //! Constructor
    //! \param[in] mu Viscosity
    //! \param[in] hK Mesh size parameter
    //! \param[in] bg Background mesh to be used
    //! \param[in] gm Geometry, assume implicit manifold to avoid closest point calcs
    //! \param[in] mesh_opts Meshing options
    StabStokesProblem(const double mu,
		      const double hK,
		      msh::StdOrphanMesh& bg,
		      geom::ImplicitManifold& gm,
		      const Meshing_Options& mesh_opts);

    //! Destructor
    virtual ~StabStokesProblem();

    //! Disable copy and assignment
    StabStokesProblem(const StabStokesProblem&) = delete;
    StabStokesProblem& operator=(const StabStokesProblem&) = delete;

    //! Helper method to set up a run
    //! Set up the mesh, elements, operartions, assembler, sparse data structures
    void SetupIteration();

    //! Helper method to setup a run without remeshing
    void SetupIterationWithoutRemeshing(const std::vector<int>& posverts,
					const std::vector<int>& poselmsnodes);

    //! Main functionality: compute velocity and pressure
    //! \param[in] bc Dirichlet bcs
    void Compute(const BCParams& bc);

    //! Helper method to finalize an iteration
    //! Deletes the mesh, elements, operations, assemblers and sparse data structures
    void FinalizeIteration();

    //! Get the viscosity
    double GetViscosity() const;
    
    //! Access the geometry
    const geom::ImplicitManifold& GetGeometry() const;

    //! Access the background mesh
    msh::StdOrphanMesh& GetBackgroundMesh();
    
    //! Access the conforming mesh
    const CoordConn& GetConformingMesh() const;

    //! Access the positive vertices
    const std::vector<int>& GetPositiveVertices() const;

    //! Access positively cut eleents and local nodes on the positive face
    const std::vector<int>& GetPositiveElmFacePairs() const;

    //! Access the solution
    const double* GetState() const;

    //! Set the state
    void SetState(const double* sol);
    
    //! Utility to plot the state
    void PlotState(const std::string filename) const;

    //! Compute forces and moments on the boundary
    //! Moments are computed about the origin
    void ComputeForcesAndMoments(double* F, double* M) const;
    
    //! Access the local to global map
    const LocalToGlobalMap& GetLocalToGlobalMap() const;

    //! Access the  element array
    const std::vector<Element*>& GetElementArray() const;
    
    //! Access the meshing options
    const Meshing_Options& GetMeshingOptions() const;

    //! Access the stabilization parameter
    const double GetStabilizationParameter() const;

    //! Compute forces along the positive facesboundary
    //! Moments are computed about the specified point
    void ComputeNetForceAndMoment(double* F, const double* X0, double* M) const;
    
  private:

    //! Helper function to setup elements, operations & sparse data structures
    void Setup2D(const bool remesh=true);
    
    //! Compute moments of tractions along the positive faces
    //! Moments are computed about the specified point
    void ComputeNetForceAndMoment2D(double* Frc,
				    const double* refX,
				    double* Moment) const;

    //! Helper function to setup elements, operations & sparse data structures
    void Setup3D(const bool remesh=true);

    //! Compute moments of tractions along the positive faces
    //! Moments are computed about the specified point
    void ComputeNetForceAndMoment3D(double* Frc,
				    const double* refX,
				    double* Moment) const;
    
    const double Mu; //!< Viscosity
    const double hVal; //!< Mesh size parameter to use for stabilization
    msh::StdOrphanMesh& BG; //!< Reference to the background mesh
    geom::ImplicitManifold& Geom; //!< Reference to the geometry
    const Meshing_Options mesh_options; //! Options to create a conforming mesh
    const int SPD; //!< Spatial dimension
    
    CoordConn MD; //!< Conforming mesh
    std::vector<int> PosVerts; //!< Positive vertics in the conforming mesh
    std::vector<int> PosElmsNodes; //!< Positive elements and local nodes on the positive face

    std::vector<Element*> ElmArray; //!<  elements
    LocalToGlobalMap* L2GMap; //!< Combined local to global map
    std::vector<stk::StabStokesOp*> OpArray; //!< Operations array
    StandardAssembler<stk::StabStokesOp>* Asm; //!< Assembler
    std::vector<double> state; //!< State solution

    PetscData PD; //!< Petsc data structures

    mutable bool is_iteration_setup;
    mutable bool is_iteration_finalized;
    mutable bool is_state_computed;
  };
}
  
#endif
