// Sriramajayam

#ifndef  STK_STABILIZED_STOKES_OPERATION_H
#define  STK_STABILIZED_STOKES_OPERATION_H

#include <ElementalOperation.h>
#include <LocalToGlobalMap.h>

namespace stk
{
  //! Assumes that the configuration is of type double*
  //! Assumes that fields are number sequentially as Ux, Uy, (Uz), P
  //! Assumes identical interpolation for velocity and pressure
  class StabStokesOp: public DResidue
  {
  public:
    //! Constructor
    //! \param[in] elmnum Element number
    //! \param[in] elm Element
    //! \param[in] mu Viscosity
    //! \param[in] hK hK for this element
    //! \param[in] l2gmap Local to global map for this element
    StabStokesOp(const int elmnum, const Element* elm, 
		 const double mu, const double hK,
		 const LocalToGlobalMap& l2gmap);

    //! Destructor
    virtual ~StabStokesOp();

    //! Copy constructor
    //! \param[in] obj Object to be copied from
    StabStokesOp(const StabStokesOp& obj);

    //! Cloning
    virtual StabStokesOp* Clone() const override;

    //! Returns the viscosity
    double GetViscosity() const;

    //! Returns the fields used
    virtual const std::vector<int>& GetField() const override;

    //! Return the element
    const Element* GetElement() const;

    //! Returns the number of dofs for a field
    int GetFieldDof(const int field) const override;
    
    //! Returns the stabilization parameter
    double GetStabilizationParameter() const;
    
    //! Returns the functional
    //! \param[in] config State at which to compute the residual
    virtual double GetEnergy(const void* state) const;
      
    //! Compute the residual vector
    //! \param[in] config State at which to compute the residual
    //! \param[out] funcval Computed elemental force vector
    virtual void GetVal(const void* state,
			std::vector<std::vector<double>>* funcval) const override;

    //! Compute the residual and stiffness
    //! \param[in] config State at which to compute the residual
    //! \param[out] funcval Computed elemental force vector
    //! \param[out] dfuncval Computed elemental stiffness matrix
    virtual void GetDVal(const void* state,
			 std::vector<std::vector<double>>* funcval,
			 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const override;

    //! Consistency test
    //! \param[in] state State at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for dof perturbations
    //! \param[in] tolEPS Tolerance to use for consistency check
    virtual bool ConsistencyTest(const void* state,
				 const double pertEPS,
				 const double tolEPS) const override;

  private:
    const int ElmNum; //!< Element number
    const Element* Elm; //!< Pointer to the element
    const double Mu; //!< Viscosity
    const LocalToGlobalMap& L2GMap; //!< Local to global map
    std::vector<int> FieldsUsed; //!< Fields used
    const double Tau; //!< Stabilization parameter
  };

}

#endif
