// Sriramajayam

#include <stk_StabilizedStokesProblem.h>
#include <MeshUtils.h>
#include <P12DElement.h>

namespace stk
{
  void StabStokesProblem::Setup2D(const bool remesh)
  {
    assert(SPD==2 &&
	   is_iteration_setup==false &&
	   is_iteration_finalized==true &&
	   is_state_computed==false);
    
    // Create a conforming mesh
    if(remesh==true)
      {
	stk::GetConformingMesh<2>(BG, Geom, mesh_options,
				  MD.coordinates, MD.connectivity, PosVerts, PosElmsNodes);
      }
    else // Simply copy the background mesh
      {
	int nNodes = BG.GetNumNodes();
	int nElements =  BG.GetNumElements();
	MD.coordinates.reserve(SPD*nNodes);
	MD.connectivity.reserve((SPD+1)*nElements);
	for(int e=0; e<nElements; ++e)
	  { const auto* conn = BG.connectivity(e);
	    for(int a=0; a<SPD+1; ++a)
	      MD.connectivity.push_back( conn[a]+1 ); }
	for(int n=0; n<nNodes; ++n)
	  { const auto* coord = BG.coordinates(n);
	    for(int k=0; k<SPD; ++k)
	      MD.coordinates.push_back( coord[k] ); }
      }
    MD.nodes_element = SPD+1;
    MD.spatial_dimension = SPD;
    MD.nodes = static_cast<int>(MD.coordinates.size()/SPD);
    MD.elements = static_cast<int>(MD.connectivity.size()/MD.nodes_element);
    
    // Create elements
    Segment<2>::SetGlobalCoordinatesArray(MD.coordinates);
    Triangle<2>::SetGlobalCoordinatesArray(MD.coordinates);
    ElmArray.resize(MD.elements);
    for(int e=0; e<MD.elements; ++e)
      { const int* conn = &MD.connectivity[3*e];
	ElmArray[e] = new P12DElement<3>(conn[0], conn[1], conn[2]); }

    // Local to global map
    L2GMap = new StandardP12DMap(ElmArray);

    // Create operations
    OpArray.resize(MD.elements);
    for(int e=0; e<MD.elements; ++e)
      OpArray[e] = new StabStokesOp(e, ElmArray[e], Mu, hVal, *L2GMap);

    // Assembler
    Asm = new StandardAssembler<StabStokesOp>(OpArray, *L2GMap);

    // State variables
    const int nTotalDofs = L2GMap->GetTotalNumDof();
    state.resize(nTotalDofs);
    std::fill(state.begin(), state.end(), 0.);

    // Sparse data structures
    std::vector<int> nnz;
    Asm->CountNonzeros(nnz);
    PD.Initialize(nnz);

    // -- done --
    return;
  }

  
  // Compute the net force and moments along positive faces
  void StabStokesProblem::ComputeNetForceAndMoment2D(double* Frc,
						     const double* refX,
						     double* Moment) const
  {
    assert(SPD==2);
    assert(Frc!=nullptr && Moment!=nullptr && refX!=nullptr);
    Frc[0] = Frc[1] = 0.;
    Moment[0] = 0.;
    
    // Shape functions
    Linear<2> Shp;
    
    // Use quadrature along each positive face
    std::vector<const Quadrature*> FaceQRules(
					      {Triangle_1::FaceOne,
						  Triangle_1::FaceTwo,
						  Triangle_1::FaceThree});

    // Neighbor list
    // Use the fact that +ve faces are along the boundary
    std::vector<std::vector<int>> ElmNbs;
    GetCoordConnFaceNeighborList(MD, ElmNbs);

    // Loop over +ve faces of +vely cut triangles
    /// Compute the traction
    // Integrate the forces and moments
    double gradU[2][2];
    double sigma[2][2];
    const double Identity[2][2] = {{1.,0.},{0.,1.}};
    double Pressure;
    double normal[2];
    double temp;
    double traction[2];
    double rvec[2];
    double RefLen;
    
    const int nPosElms = static_cast<int>(PosElmsNodes.size()/3);
    for(int i=0; i<nPosElms; ++i)
      {
	const int e = PosElmsNodes[3*i];
	const int& a0 = PosElmsNodes[3*i+1];
	const int& a1 = PosElmsNodes[3*i+2];
	
	assert(ElmNbs[e][0]<0 || ElmNbs[e][2]<0 || ElmNbs[e][4]<0); // At least one free face

	const int* conn = &MD.connectivity[3*e];
	const int n0 = conn[a0]-1;
	const int n1 = conn[a1]-1;
	const double* X = &MD.coordinates[SPD*n0];
	const double* Y = &MD.coordinates[SPD*n1];
	const double len = std::sqrt((X[0]-Y[0])*(X[0]-Y[0])+(X[1]-Y[1])*(X[1]-Y[1]));

	// Find the free face
	for(int f=0; f<3; ++f)
	  if(ElmNbs[e][2*f]<0)
	    {
	      // This is a positive face
	      // Evaluate shape functions and derivatives at quadrature points on this face
	      const auto& ElmGeom = ElmArray[e]->GetElementGeometry();
	      ShapesEvaluated ShpEval(FaceQRules[f], &Shp, &ElmGeom);

	      // Shape functions and derivatives
	      const auto& ShpVals = ShpEval.GetShapes();
	      const auto& DShpVals = ShpEval.GetDShapes();
	      const int ndof = ShpEval.GetBasisDimension(); assert(ndof==3);

	      // Integration points & weights
	      const int nQuad = FaceQRules[f]->GetNumberQuadraturePoints();
	      const auto& Qpts = ShpEval.GetQuadraturePointCoordinates();
	      assert(static_cast<int>(Qpts.size())==SPD*nQuad);

	      // Length of reference element
	      RefLen = 0.;
	      for(int q=0; q<nQuad; ++q)
		RefLen += FaceQRules[f]->GetQuadratureWeights(q);
	      assert(std::abs(RefLen-1.)<1.e-6);
	      
	      for(int q=0; q<nQuad; ++q)
		{
		  // Quadrature weight
		  const double Qwt = (len/RefLen)*FaceQRules[f]->GetQuadratureWeights(q);

		  // Pressure
		  Pressure = 0.;
		  for(int a=0; a<ndof; ++a)
		    Pressure += state[L2GMap->Map(SPD,a,e)]*ShpVals[ndof*q+a];

		  // grad(U)
		  for(int k=0; k<SPD; ++k)
		    for(int L=0; L<SPD; ++L)
		      { gradU[k][L] = 0.;
			for(int a=0; a<ndof; ++a)
			  gradU[k][L] += state[L2GMap->Map(k,a,e)]*DShpVals[SPD*ndof*q+SPD*a+L]; }
		  
		  // Stress
		  for(int k=0; k<SPD; ++k)
		    for(int L=0; L<SPD; ++L)
		      sigma[k][L] = Mu*0.5*(gradU[k][L]+gradU[L][k]) - Pressure*Identity[k][L];

		  // Normal to the boundary at this quadrature point
		  Geom.GetSignedDistance(&Qpts[SPD*q], temp, normal);
		  temp = std::sqrt(normal[0]*normal[0]+normal[1]*normal[1]);
		  assert(temp>1.e-2);
		  normal[0] /= temp;
		  normal[1] /= temp;

		  // Traction
		  for(int k=0; k<SPD; ++k)
		    { traction[k] = 0.;
		      for(int L=0; L<SPD; ++L)
			traction[k] += sigma[k][L]*normal[L]; }

		  // Integrate the traction
		  for(int k=0; k<SPD; ++k)
		    Frc[k] += Qwt*traction[k];
		  
		  // Integrate the moment
		  rvec[0] = Qpts[SPD*q+0]-refX[0];
		  rvec[1] = Qpts[SPD*q+1]-refX[1];
		  Moment[0] += Qwt*(rvec[0]*traction[1]-rvec[1]*traction[0]);
		}
	    }
      }
    return;
  }
}
