
#include <stk_SeqPairedL2GMap.h>
#include <cassert>

namespace stk
{
  // Constructor
  SeqPairedL2GMap::SeqPairedL2GMap(const LocalToGlobalMap& map1,
				   const LocalToGlobalMap& map2)
    :L2GMapA(map1),
     L2GMapB(map2),
     nTotalDofA(map1.GetTotalNumDof())
  {
    assert(L2GMapA.GetNumElements()==L2GMapB.GetNumElements() &&
	   "stk::SeqPairedL2GMap- maps should have identical number of elements");
  }
		
  // Destructor
  SeqPairedL2GMap::~SeqPairedL2GMap() {}
  
  // Copy constructor
  SeqPairedL2GMap::SeqPairedL2GMap(const SeqPairedL2GMap& Obj)
    :L2GMapA(Obj.L2GMapA),
     L2GMapB(Obj.L2GMapB),
     nTotalDofA(Obj.nTotalDofA) {}

  // Cloning
  SeqPairedL2GMap* SeqPairedL2GMap::Clone() const
  { return new SeqPairedL2GMap(*this); }
    
  // Returns the number of elements
  int SeqPairedL2GMap::GetNumElements() const
  { return L2GMapA.GetNumElements(); }

  // Returns the number of fields by gathering from each map
  int SeqPairedL2GMap::GetNumFields(const int elm) const
  { return L2GMapA.GetNumFields(elm) + L2GMapB.GetNumFields(elm); }

  // Returns the number of dofs for a given field
  int SeqPairedL2GMap::GetNumDofs(const int elm, const int field) const
  { 
    int nFieldsA = static_cast<int>(L2GMapA.GetNumFields(elm));
    if(field<nFieldsA)
      return L2GMapA.GetNumDofs(elm, field);
    else
      return L2GMapB.GetNumDofs(elm, field-nFieldsA);
  }
  
  // Returns the total number of dofs
  int SeqPairedL2GMap::GetTotalNumDof() const
  { return L2GMapA.GetTotalNumDof() + L2GMapB.GetTotalNumDof(); }

  // Maps the local dof to the global dof
  int SeqPairedL2GMap::Map(const int field, const int dof, const int elm) const
  {
    int nFieldsA = L2GMapA.GetNumFields(elm);
    if(field<nFieldsA)
      return L2GMapA.Map(field, dof, elm);
    else
      return nTotalDofA + L2GMapB.Map(field-nFieldsA, dof, elm);
  }
}
