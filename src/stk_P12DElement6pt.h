
#ifndef STK_P12DELEMENT_6PT
#define STK_P12DELEMENT_6PT

#include <P12DElement.h>
#include <TriangleQuadratures.h>

namespace stk
{
  template<int NFields>
    class P12DElement6pt: public Element
    {
    public:
    P12DElement6pt(int i1, int i2, int i3)
      :Element()
	{
	  TriGeom = new Triangle<2>(i1,i2,i3);
	  Linear<2> Shp;
	  ShapesEvaluated ModelShape(Triangle_6pt::Bulk, &Shp, TriGeom);
	  AddBasisFunctions(ModelShape);
	  for(int i=0; i<NFields; ++i)
	    AppendField(i);
	}
  
      virtual ~P12DElement6pt() { delete TriGeom; }
  
    P12DElement6pt(const P12DElement6pt<NFields> &Elm)
      :Element(Elm)
      { TriGeom = new Triangle<2>(*Elm.TriGeom); }

      //! Cloning
      inline virtual P12DElement6pt<NFields>* Clone() const override
      { return new P12DElement6pt<NFields>(*this); }
  
      virtual const Triangle<2>& GetElementGeometry() const override
      { return *TriGeom; }
  
    protected:
      //! Mapping from field to its index in this element
      //! Multiple fields can use the same basis functions.
      //! Therefore, the index need not be consistent with the member Fields_
      //! \param[in] field Global field number
      //! \return Index of given field
      virtual int GetFieldIndex(int field) const override
      { return 0; }
  
    private:
      Triangle<2> *TriGeom;
    };
}

#endif
