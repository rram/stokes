
#ifndef P1_BUBBLE_L2GMAP_H
#define P1_BUBBLE_L2GMAP_H

#include <P12DElement.h>

namespace stk
{
  //! All nodal dofs are ordered first, followed
  //! by the bubble dofs 
  class P1BubbleL2GMap: public StandardP12DMap
  {
  public:
    //! Constructor
    //! \param[in] EA Elemnt array
    P1BubbleL2GMap(const std::vector<Element*>& EA);
      
    //! Destructor
    virtual ~P1BubbleL2GMap();

    //! Copy constructor
    //! \param[in] obj Object to be copied
    P1BubbleL2GMap(const P1BubbleL2GMap& obj);
      
    //! Cloning
    virtual P1BubbleL2GMap* Clone() const override;

    //! Main functionality- map local to global dofs
    int Map(const int field, const int dof, const int elm) const override;

    //! Returns the total number of dofs
    virtual int GetTotalNumDof() const override;

  private:
    const std::vector<Element*>* ElmArray; //! Pointer to the element array
    const int SPD; //!< Sptial dimension (2D or 3D)
    const int nElements; //!< Number of elements
    int nNodalDofs; //!< Number of nodal dofs
    int bubble_dof; //!< Bubble dof number in the element
  };
}

#endif
