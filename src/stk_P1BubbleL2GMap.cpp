
#include <stk_P1BubbleL2GMap.h>
#include <cassert>

namespace stk
{
  // Constructor
  P1BubbleL2GMap::P1BubbleL2GMap(const std::vector<Element*>& EA)
    :StandardP12DMap(EA),
     ElmArray(&EA),
     SPD(EA[0]->GetNumDerivatives(0)), // spatial dimension
     nElements(static_cast<int>(EA.size()))
  {
    assert((SPD==2 || SPD==3) &&
	   "std::P1BubbleL2GMap- expected spatial dimension to be 2 or 3.");
    nNodalDofs = StandardP12DMap::GetTotalNumDof();
    bubble_dof = SPD+1;
  }
	  
  // Destructor
  P1BubbleL2GMap::~P1BubbleL2GMap() {}
  
  // Copy constructor
  P1BubbleL2GMap::P1BubbleL2GMap(const P1BubbleL2GMap& obj)
    :StandardP12DMap(obj),
     ElmArray(obj.ElmArray),
     SPD(obj.SPD),
     nElements(obj.nElements),
     nNodalDofs(obj.nNodalDofs),
     bubble_dof(obj.bubble_dof) {}
  
  // Cloning
  P1BubbleL2GMap* P1BubbleL2GMap::Clone() const
  { return new P1BubbleL2GMap(*this); }
  
  // Returns the total number of dofs
  int P1BubbleL2GMap::GetTotalNumDof() const
  { return nNodalDofs + SPD*nElements; }
  
  // Main functionality- map local to global dofs
  int P1BubbleL2GMap::Map(const int field, const int dof, const int elm) const
  {
    // If bubble: total nodal dofs + element-wise-numbering
    if(dof==bubble_dof)
      return nNodalDofs + SPD*elm + field; 
    else
      // This is a nodal dof
      return StandardP12DMap::Map(field, dof, elm);
  }
}
