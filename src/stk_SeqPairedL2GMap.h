
#ifndef STK_SEQ_PAIRED_L2GMAP_H
#define STK_SEQ_PAIRED_L2GMAP_H

#include <LocalToGlobalMap.h>

namespace stk
{
  class SeqPairedL2GMap: public LocalToGlobalMap
  {
  public:
    //! Constructor
    //! \param[in] map1 First map
    //! \param[in] map2 Second map
    SeqPairedL2GMap(const LocalToGlobalMap& map1,
		    const LocalToGlobalMap& map2);

    //! Destructor
    virtual ~SeqPairedL2GMap();

    //! Copy constructor
    //! \param Obj Object to be copied
    SeqPairedL2GMap(const SeqPairedL2GMap& Obj);

    //! Cloning
    virtual SeqPairedL2GMap* Clone() const override;
    
    //! Returns the number of elements
    virtual int GetNumElements() const override;

    //! Returns the number of fields by gathering from each map
    //! \param[in] elm Element number
    virtual int GetNumFields(const int elm) const override;

    //! Returns the number of dofs for a given field
    //! \param[in] elm Element number
    //! \param[in] field Field number. Assumed to be sequential in both maps
    virtual int GetNumDofs(const int elm, const int field) const override;

    //! Returns the total number of dofs
    virtual int GetTotalNumDof() const override;

    //! Maps the local dof to the global dof
    //! \param[in] field Field number
    //! \param[in] dof Dof number
    //! \param[in] elm Element number
    virtual int Map(const int field, const int dof, const int elm) const;

  private:
    const LocalToGlobalMap& L2GMapA;
    const LocalToGlobalMap& L2GMapB;
    const int nTotalDofA;
  };
}

#endif
