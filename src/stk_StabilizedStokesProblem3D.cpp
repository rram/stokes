// Sriramajayam

#include <stk_StabilizedStokesProblem.h>
#include <MeshUtils.h>
#include <P13DElement.h>

namespace stk
{
  void StabStokesProblem::Setup3D(const bool remesh)
  {
    assert(SPD==3 &&
	   is_iteration_setup==false &&
	   is_iteration_finalized==true &&
	   is_state_computed==false);

    if(remesh==true)
      {
	// Create a conforming mesh
	stk::GetConformingMesh<3>(BG, Geom, mesh_options,
				  MD.coordinates, MD.connectivity, PosVerts, PosElmsNodes);
      }
    else // Simply copy the background mesh
      {
	int nNodes = BG.GetNumNodes();
	int nElements =  BG.GetNumElements();
	MD.coordinates.reserve(SPD*nNodes);
	MD.connectivity.reserve((SPD+1)*nElements);
	for(int e=0; e<nElements; ++e)
	  { const auto* conn = BG.connectivity(e);
	    for(int a=0; a<SPD+1; ++a)
	      MD.connectivity.push_back( conn[a]+1 ); }
	for(int n=0; n<nNodes; ++n)
	  { const auto* coord = BG.coordinates(n);
	    for(int k=0; k<SPD; ++k)
	      MD.coordinates.push_back( coord[k] ); }
      }
    MD.nodes_element = SPD+1;
    MD.spatial_dimension = SPD;
    MD.nodes = static_cast<int>(MD.coordinates.size()/SPD);
    MD.elements = static_cast<int>(MD.connectivity.size()/MD.nodes_element);

    // Create elements
    Segment<3>::SetGlobalCoordinatesArray(MD.coordinates);
    Triangle<3>::SetGlobalCoordinatesArray(MD.coordinates);
    Tetrahedron::SetGlobalCoordinatesArray(MD.coordinates);
    ElmArray.resize(MD.elements);
    for(int e=0; e<MD.elements; ++e)
      { const int* conn = &MD.connectivity[4*e];
	ElmArray[e] = new P13DElement<4>(conn[0], conn[1], conn[2], conn[3]); }

    // Local to global map
    L2GMap = new StandardP13DMap(ElmArray);

    // Create operations
    OpArray.resize(MD.elements);
    for(int e=0; e<MD.elements; ++e)
      OpArray[e] = new StabStokesOp(e, ElmArray[e], Mu, hVal, *L2GMap);

    // Assembler
    Asm = new StandardAssembler<StabStokesOp>(OpArray, *L2GMap);

    // State variables
    const int nTotalDofs = L2GMap->GetTotalNumDof();
    state.resize(nTotalDofs);
    std::fill(state.begin(), state.end(), 0.);

    // Sparse data structures
    std::vector<int> nnz({});
    Asm->CountNonzeros(nnz);
    PD.Initialize(nnz);

    // -- done --
    return;
  }


  // Compute the area of a triangle
  namespace
  {
    // Compute the area of triangle ABC embedded in 3D
    double TriangleArea(const double* A, const double* B, const double* C)
    {
      double l1 = 0.;
      double l2 = 0.;
      double l3 = 0.;
      for(int k=0; k<3; ++k)
	{ l1 += (A[k]-B[k])*(A[k]-B[k]);
	  l2 += (B[k]-C[k])*(B[k]-C[k]);
	  l3 += (C[k]-A[k])*(C[k]-A[k]); }
      l1 = std::sqrt(l1);
      l2 = std::sqrt(l2);
      l3 = std::sqrt(l3);
      double s = 0.5*(l1+l2+l3);
      return std::sqrt(s*(s-l1)*(s-l2)*(s-l3));
    }
  }


  // Compute the net force and moments along positive faces
  void StabStokesProblem::ComputeNetForceAndMoment3D(double* Frc,
						     const double* refX,
						     double* Moment) const
  {
    assert(SPD==3);
    assert(Frc!=nullptr && Moment!=nullptr&& refX!=nullptr);
    for(int k=0; k<SPD; ++k)
      { Frc[k] = 0.; Moment[k] = 0.; }

    // Shape functions
    const int perm[] = {0,1,3,2};
    Linear<3> Shp(perm);

    // Use quadrature along positive faces
    // Faces are numbered consistently in quadrature & neighbor lists as:
    // face 0: 2,1,0
    // face 1: 2,0,3
    // face 2: 2,3,1
    // face 3: 0,1,3
    std::vector<const Quadrature*> FaceQRules(
					      {Tet_1::FaceOne,
						  Tet_1::FaceTwo,
						  Tet_1::FaceThree,
						  Tet_1::FaceFour});
    
    // Neighbor list: use the fact that +ve faces are along the boundary
    std::vector<std::vector<int>> ElmNbs;
    GetCoordConnFaceNeighborList(MD, ElmNbs);

    // Loop over +ve faces of +vely cut tets
    // Compute the traction
    // Integrate the forces and moments
    double gradU[3][3];
    double sigma[3][3];
    const double Identity[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
    double Pressure;
    double normal[3];
    double temp;
    double traction[3];
    double rvec[3];
    double RefArea;

    const int nPosElms = static_cast<int>(PosElmsNodes.size()/4);
    for(int i=0; i<nPosElms; ++i)
      {
	const int e = PosElmsNodes[4*i];
	const int& a0 = PosElmsNodes[4*i+1];
	const int& a1 = PosElmsNodes[4*i+2];
	const int& a2 = PosElmsNodes[4*i+3];
	
	assert(ElmNbs[e][0]<0 || ElmNbs[e][2]<0 ||
	       ElmNbs[e][4]<0 || ElmNbs[e][6]<0); // At least one free face

	const int* conn = &MD.connectivity[4*e];
	const int n0 = conn[a0]-1;
	const int n1 = conn[a1]-1;
	const int n2 = conn[a2]-1;
	const double* X = &MD.coordinates[SPD*n0];
	const double* Y = &MD.coordinates[SPD*n1];
	const double* Z = &MD.coordinates[SPD*n2];

	// Compute the area of triangle XYZ
	const double Area = TriangleArea(X,Y,Z);

	// Find the free face
	for(int f=0; f<4; ++f)
	  if(ElmNbs[e][2*f]<0)
	    {
	      // This is a positive face
	      // Evaluate shape functions and derivatives at quadrature points on this face
	      const auto& ElmGeom = ElmArray[e]->GetElementGeometry();
	      ShapesEvaluated ShpEval(FaceQRules[f], &Shp, &ElmGeom);

	      // Shape functions and derivatives
	      const auto& ShpVals = ShpEval.GetShapes();
	      const auto& DShpVals = ShpEval.GetDShapes();
	      const int ndof = ShpEval.GetBasisDimension(); assert(ndof==4);

	      // Integration points & weights
	      const int nQuad = FaceQRules[f]->GetNumberQuadraturePoints();
	      const auto& Qpts = ShpEval.GetQuadraturePointCoordinates();
	      assert(static_cast<int>(Qpts.size())==SPD*nQuad);

	      // Area of reference triangle
	      RefArea = 0.;
	      for(int q=0; q<nQuad; ++q)
		RefArea += FaceQRules[f]->GetQuadratureWeights(q);
	      assert(std::abs(RefArea-0.5)<1.e-6);

	      for(int q=0; q<nQuad; ++q)
		{
		  // Quadrature weight
		  const double Qwt = (Area/RefArea)*FaceQRules[f]->GetQuadratureWeights(q);

		  // Pressure
		  Pressure = 0.;
		  for(int a=0; a<ndof; ++a)
		    Pressure += state[L2GMap->Map(SPD, a, e)]*ShpVals[ndof*q+a];

		  // grad(U)
		  for(int k=0; k<SPD; ++k)
		    for(int L=0; L<SPD; ++L)
		      { gradU[k][L] = 0.;
			for(int a=0; a<ndof; ++a)
			  gradU[k][L] += state[L2GMap->Map(k,a,e)]*DShpVals[SPD*ndof*q+SPD*a+L]; }

		  // Stress
		  for(int k=0; k<SPD; ++k)
		    for(int L=0; L<SPD; ++L)
		      sigma[k][L] = Mu*0.5*(gradU[k][L]+gradU[L][k]) - Pressure*Identity[k][L];

		  // Normal to the boundary at this quadrature point
		  Geom.GetSignedDistance(&Qpts[SPD*q], temp, normal);
		  temp = std::sqrt(normal[0]*normal[0]+normal[1]*normal[1]+normal[2]*normal[2]);
		  assert(temp>1.e-2);
		  normal[0] /= temp;
		  normal[1] /= temp;
		  normal[2] /= temp;
		  
		  // Traction
		  for(int k=0; k<SPD; ++k)
		    { traction[k] = 0.;
		      for(int L=0; L<SPD; ++L)
			traction[k] += sigma[k][L]*normal[L]; }
		  
		  // Integrate the traction
		  for(int k=0; k<SPD; ++k)
		    Frc[k] += Qwt*traction[k];
		  
		  // Integrate the moment
		  rvec[0] = Qpts[SPD*q+0]-refX[0];
		  rvec[1] = Qpts[SPD*q+1]-refX[1];
		  rvec[2] = Qpts[SPD*q+2]-refX[2];
		  for(int k=0; k<SPD; ++k)
		    Moment[k] += Qwt*(rvec[(k+1)%SPD]*traction[(k+2)%SPD]-
				      rvec[(k+2)%SPD]*traction[(k+1)%SPD]);
		}
	    }
      }
    return;
  }	   
}
