
#ifndef STK_MESH_UTILS_H
#define STK_MESH_UTILS_H

#include <geom_ImplicitManifold.h>
#include <msh_TriangleModule>
#include <msh_TetrahedronModule>
#include <list>

namespace stk
{
  // Struct with options for meshing
  struct Meshing_Options
  {
    int nMaxThreads; //!< Number of threads to use
    int MaxVertValency; //!< Estimated max number of vertices in a 1-ring
    int MaxElmValency; //!< Estimated max number of elements in a 1-ring
    int nSteps; //!< Number of projection steps
    int nIters; //!< Number of relaxation iterations
    int nBdSamples; //!< Number of samples for boundary relaxation
  };

  //! Interface for computing a mesh conforming to a given implicit manifold
  //! Connectivities are numbered from 1
  template<int SPD>
    void GetConformingMesh(msh::StdOrphanMesh& BG,
			   geom::ImplicitManifold& Geom,
			   const Meshing_Options& options,
			   std::vector<double>& coordinates,
			   std::vector<int>& connectivity,
			   std::vector<int>& posverts,
			   std::vector<int>& poselmsnodes);
}

#endif
